package br.com.ia.oct.mcd.remedy;

import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;

import br.com.ia.oct.mcd.common.utils.Configuration;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SendSFTP {

	private static final Logger LOG = Logger.getLogger(SendSFTP.class);
	
	public void send(String fileName) {
	      
		String SFTPHOST = Configuration.getConfig("mcd.sftp.host");
        int SFTPPORT = 22;
        String SFTPUSER = Configuration.getConfig("mcd.sftp.username");
        String SFTPPASS = Configuration.getConfig("mcd.sftp.password");
        String SFTPWORKINGDIR = Configuration.getConfig("mcd.sftp.adress");

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            LOG.debug("[SendSFTP] Host conectado.");
            channel = session.openChannel("sftp");
            channel.connect();
            LOG.debug("[SendSFTP] sftp conectado.");
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            File f = new File(fileName);
            channelSftp.put(new FileInputStream(f), f.getName());
            LOG.info("Sucesso ao enviar arquivo CSV.");
        } catch (Exception ex) {
             LOG.error("Erro ao enviar o arquivo CSV via SFTP. Mensagem:" +ex.getMessage());
        }
        finally{
            channelSftp.exit();
            channel.disconnect();
            session.disconnect();
        }
    }  
	
}
