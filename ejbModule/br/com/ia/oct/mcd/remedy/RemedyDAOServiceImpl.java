package br.com.ia.oct.mcd.remedy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.DataType;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;

import br.com.ia.oct.mcd.common.annotations.EntityProp;
import br.com.ia.oct.mcd.common.annotations.FieldProp;
import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.entity.remedy.RemedyAttachment;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.OCTUtils;

@Stateful
@Remote(RemedyDAOService.class)
@Local(RemedyDAOServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/daoservice")
@OCTComponent(version="$LastChangedRevision$",uid="4.5.2", nome="RemedyDAOServiceImpl", descricao="Componente de acesso a dados dos formulários do Remedy", jndiLookup="MCD/RemedyDAOServiceImpl/local", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="REMEDY", ativo=true)
public class RemedyDAOServiceImpl implements RemedyDAOService, RemedyDAOServiceLocal {
	private static final Logger LOG = Logger.getLogger(RemedyDAOServiceImpl.class);
	
	private static ARServerUser ars;
	private static String REMEDY_USER 	= "";
	private static String REMEDY_PASSWD 	= "";
	private static String REMEDY_SERVER 	= "";
	private static int 	  REMEDY_PORT 	= 0;
	
	@PostConstruct
	public void inicializa () {
		REMEDY_USER   = Configuration.getConfig("mcd.remedy.params.username");
		REMEDY_PASSWD = Configuration.getConfig("mcd.remedy.params.password");
		REMEDY_SERVER = Configuration.getConfig("mcd.remedy.params.server");
		REMEDY_PORT   = Integer.parseInt(Configuration.getConfig("mcd.remedy.params.port"));
		OCTUtils.setProxyConfig();
	}
	
	@PreDestroy
	public void finaliza () {
		if (ars != null) {
			String userSessionGuid = ars.getUserSessionGuid();
			
			if (userSessionGuid != null) {
				ars.logout();	
				ars = null;
			}
		}
	}
	
	private void login(String user, String passwd, String server, int port, int rpc) throws Exception {
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.setPort(port);
				_ars.usePrivateRpcQueue(rpc);
				_ars.login();
				ars = _ars;
			}			
			LOG.info("Login Remedy Realizado com Sucesso!!");
		} catch(ARException e){
			LOG.error("Erro no login!!! " + e.getMessage());
			throw new EJBException(e.getMessage());
		}
	}
	
	private void login(String user, String passwd, String server, int port) throws Exception {
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.setPort(port);
				_ars.login();
				ars = _ars;
			}
			LOG.info("Login Remedy Realizado com Sucesso!! ");
		} catch(ARException e){
			LOG.error("Erro no login!!! " + e.getMessage());
			throw new EJBException(e.getMessage());
		}
	}
	
	private void login(String user, String passwd, String server) throws Exception {
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.login();
				ars = _ars;
			}
			LOG.info("Login Remedy Realizado com Sucesso!! ");
		} catch(ARException e){
			LOG.error("Erro no login!!! " + e.getMessage());
			throw new EJBException(e.getMessage());
		}
	}
	
	private void logout() {
		ars.logout();
	}
	
	public boolean createListEntry(String formName,List<HashMap<Integer, Object>> object) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			
			for(HashMap<Integer, Object> registro : object){
				Entry entry = new Entry();
				Set<java.util.Map.Entry<Integer, Object>> entrySet = registro.entrySet();
				Iterator<java.util.Map.Entry<Integer, Object>> iterator = entrySet.iterator();
				while(iterator.hasNext()){
					java.util.Map.Entry<Integer, Object> next = iterator.next();
					if(next.getValue() instanceof Integer){
						Integer obj = (Integer) next.getValue();
						entry.put(next.getKey(), new Value(obj));
					} else if (next.getValue() instanceof String){
						String obj = (String) next.getValue();
						entry.put(next.getKey(), new Value(obj));
					} else if (next.getValue() instanceof Calendar){
						Calendar obj = (Calendar) next.getValue();
						entry.put(next.getKey(), new Value(new Timestamp(obj.getTime())));
					} else if(next.getValue() instanceof RemedyAttachment) {
						RemedyAttachment att = (RemedyAttachment) next.getValue();
						AttachmentValue v = new AttachmentValue(att.getByteArray());
						v.setName(att.getName());
						entry.put(next.getKey(), new Value(v));
					}
				}
				String createEntry = ars.createEntry(formName, entry);
				LOG.debug("Registro Remedy: " + createEntry);
			}
			
			this.logout();
			return true;
		} catch (ARException e) {
			LOG.error("ARException", e);
			throw new EJBException(e.getMessage());
		} catch (Exception e) {
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}
	
	private String createRegistro(String formName, HashMap<Integer, Object> valores) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			Entry entry = new Entry();
			Set<java.util.Map.Entry<Integer, Object>> entrySet = valores.entrySet();
			Iterator<java.util.Map.Entry<Integer, Object>> iterator = entrySet.iterator();
			while(iterator.hasNext()){
				java.util.Map.Entry<Integer, Object> next = iterator.next();
				if(next.getValue() instanceof Integer){
					Integer obj = (Integer) next.getValue();
					entry.put(next.getKey(), new Value(obj));
				} else if (next.getValue() instanceof String){
					String obj = (String) next.getValue();
					entry.put(next.getKey(), new Value(obj));
				} else if (next.getValue() instanceof Calendar){
					Calendar obj = (Calendar) next.getValue();
					entry.put(next.getKey(), new Value(new Timestamp(obj.getTime())));
				} else if(next.getValue() instanceof RemedyAttachment) {
					RemedyAttachment att = (RemedyAttachment) next.getValue();
					AttachmentValue v = new AttachmentValue(att.getByteArray());
					v.setName(att.getName());
					entry.put(next.getKey(), new Value(v));
				}
			}
			String createEntry = ars.createEntry(formName, entry);
			this.logout();
			LOG.debug("Registro Remedy: " + createEntry);
			return createEntry;
		} catch (ARException e) {
			LOG.error("ARException", e);
			throw new EJBException(e.getMessage());
		} catch (Exception e) {
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}

	public Boolean isAlive() {
		return true;
	}
	
	public String createEntry(String formName, HashMap<Integer, Object> object) throws Exception {
		return this.createRegistro(formName, object);
	}
	
	public boolean updateEntry(String formName, String requestId, HashMap<String, Object> object) throws Exception {
		boolean ret = false;

		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			Entry e = new Entry();
			List<Field> listFieldObjects = ars.getListFieldObjects(formName);

			for(Field f : listFieldObjects){
				if(object.containsKey(f.getName())){
					if(object.get(f.getName()) instanceof Integer){
						Integer obj = (Integer) object.get(f.getName());
						e.put(f.getFieldID(), new Value(obj) );
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					} else if (object.get(f.getName()) instanceof String){
						String obj = (String) object.get(f.getName());
						e.put(f.getFieldID(), new Value(obj) );
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					} else if (object.get(f.getName()) instanceof Calendar){
						Calendar obj = (Calendar) object.get(f.getName());
						e.put(f.getFieldID(), new Value(new Timestamp(obj.getTime())));
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					}
				}
			}
			
			ars.setEntry(formName, requestId, e, null, 0);
			this.logout();
			ret = true;
		} catch (ARException arEx){
			LOG.error(arEx);			
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}

		return ret;
	}

	public boolean deleteEntry(String formName, String requestId) {
		return false;
	}
	
	public List<HashMap<String, String>> queryEntry(String formName, String query, HashMap<Integer, String> campos) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			int[] fieldId = new int[campos.size()];
			Iterator<Integer> iterator = campos.keySet().iterator();
			int a=0;
			while(iterator.hasNext()){
				fieldId[a] = iterator.next();
				a++;
			}
			
			System.out.println("Iniciando a busca no form " + formName + ". [Query - " + query);
			List<Entry> listEntryObjects = ars.getListEntryObjects(formName, ars.parseQualification(formName,query),0,1000,null,fieldId,false,null);
			System.out.println("Quantidade de registros retornados: " + listEntryObjects.size());
			
			List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();
			for(Entry e : listEntryObjects){
				HashMap<String, String> r = new HashMap<String, String>();
				for(int c=0;c<fieldId.length;c++){
					Field field = ars.getField(formName, fieldId[c]);
					Value v = e.get(field.getFieldID());
					if(v.getValue() == null){
						r.put(field.getName(), null);
//						System.out.println("Field: " + field.getName() + " - Value: " + null);
					} else {
						if(v.getDataType().equals(DataType.CURRENCY)){
							r.put(field.getName(), v.getCurrencyValue());
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getCurrencyValue());
						} else if(v.getDataType().equals(DataType.INTEGER)){
							r.put(field.getName(), Integer.toString(v.getIntValue()));
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getIntValue());
						} else {
							r.put(field.getName(), v.getValue().toString());
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getValue().toString());
						}
					}
				}
				retorno.add(r);			
			}
			this.logout();
			return retorno;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
		
	}
	
	public List<HashMap<String, String>> queryEntry(String formName, String query, HashMap<Integer, String> campos, Integer limit) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			int[] fieldId = new int[campos.size()];
			Iterator<Integer> iterator = campos.keySet().iterator();
			int a=0;
			while(iterator.hasNext()){
				fieldId[a] = iterator.next();
				a++;
			}
			
			System.out.println("Iniciando a busca no form " + formName + ". [Query - " + query);
			List<Entry> listEntryObjects = ars.getListEntryObjects(formName, ars.parseQualification(formName,query),0,limit,null,fieldId,false,null);
			System.out.println("Quantidade de registros retornados: " + listEntryObjects.size());
			
			List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();
			for(Entry e : listEntryObjects){
				HashMap<String, String> r = new HashMap<String, String>();
				for(int c=0;c<fieldId.length;c++){
					Field field = ars.getField(formName, fieldId[c]);
					Value v = e.get(field.getFieldID());
					if(v.getValue() == null){
						r.put(field.getName(), null);
//						System.out.println("Field: " + field.getName() + " - Value: " + null);
					} else {
						if(v.getDataType().equals(DataType.CURRENCY)){
							r.put(field.getName(), v.getCurrencyValue());
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getCurrencyValue());
						} else if(v.getDataType().equals(DataType.INTEGER)){
							r.put(field.getName(), Integer.toString(v.getIntValue()));
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getIntValue());
						} else {
							r.put(field.getName(), v.getValue().toString());
//							System.out.println("Field: " + field.getName() + " - Value: " + v.getValue().toString());
						}
					}
				}
				retorno.add(r);			
			}
			this.logout();
			return retorno;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
		
	}	
	
	public String getEntryFieldValue(String formName, String requestId,int fieldId) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			int campos[] = new int[]{fieldId};
			Entry entry = ars.getEntry(formName, requestId, campos);
			String retorno = entry.get(campos[0]).getValue().toString();
			return retorno;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}
	
	public HashMap<Integer, String> getAllFieldNames(String formName) throws Exception {
		try {
			HashMap<Integer, String> fieldReturn = new HashMap<Integer, String>();
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			List<Field> listFieldObjects = ars.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA);
			for(Field f : listFieldObjects){
				if(f.getFieldID() != 15)
					fieldReturn.put(f.getFieldID(),f.getName());
			}
			this.logout();
			return fieldReturn;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}
	
	public HashMap<Integer, List<Object>> getAllFieldNamesWithDetails(String formName) throws Exception {
		try {
			HashMap<Integer, List<Object>> fieldReturn = new HashMap<Integer, List<Object>>();
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			List<Field> listFieldObjects = ars.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA);
			for(Field f : listFieldObjects){
				if(f.getFieldID() != 15) {
					List<Object> valores = new ArrayList<Object>();
					valores.add(f.getName());
					valores.add(f.getDefaultValue());
					valores.add(f.getFieldLimit());
					valores.add(DataType.toDataType(f.getDataType()).toString());

					fieldReturn.put(f.getFieldID(), valores);
				}
			}
			this.logout();
			return fieldReturn;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}	
	
	public HashMap<Integer, Object> convertObjectToHash(Object obj) throws Exception {
		try {
			HashMap<Integer, Object> valores = new HashMap<Integer, Object>();
			Class<?> classe = obj.getClass();
			for (java.lang.reflect.Field f : classe.getDeclaredFields()){
				f.setAccessible(true);
				if( f.isAnnotationPresent(FieldProp.class)){
					FieldProp fieldProp = f.getAnnotation(FieldProp.class);
					if(fieldProp.isFile()){
						
					} else {
						int fieldId = fieldProp.fieldID();
						Object valor = f.get(obj);
						valores.put(fieldId, valor);	
					}
				}
			}
			return valores;
		} catch(IllegalArgumentException e){
			LOG.error("IllegalArgumentException", e);
			throw new Exception(e.getMessage(), e);
		} catch(IllegalAccessException e){
			LOG.error("IllegalAccessException", e);
			throw new Exception(e.getMessage(), e);
		}
	}
	
	public String getFormName(Object obj) throws Exception {
		Class<?> classe = obj.getClass();
		return classe.getAnnotation(EntityProp.class).formName();
	}
	
	public HashMap<String, Object> convertObjectToHashStringKey(Object obj)throws Exception {
		try {
			HashMap<String, Object> valores = new HashMap<String, Object>();
			Class<?> classe = obj.getClass();
			HashMap<Integer, String> allFieldNames = getAllFieldNames (classe.getAnnotation(EntityProp.class).formName());
			for (java.lang.reflect.Field f : classe.getDeclaredFields()){
				f.setAccessible(true);
				if( f.isAnnotationPresent(FieldProp.class)){
					FieldProp fieldProp = f.getAnnotation(FieldProp.class);
					if(fieldProp.isFile()){

					} else {
						String fieldName = allFieldNames.get(fieldProp.fieldID()).toString();
						Object valor = f.get(obj);
						valores.put(fieldName, valor);	
					}
				}
			}
			return valores;
		} catch(IllegalArgumentException e){
			LOG.error("IllegalArgumentException", e);
			throw new Exception(e.getMessage(), e);
		} catch(IllegalAccessException e){
			LOG.error("IllegalAccessException", e);
			throw new Exception(e.getMessage(), e);
		}
	}

	public List<String> formList() throws Exception {
		this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
		
		return ars.getListForm();
	}
	
}
