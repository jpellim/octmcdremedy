package br.com.ia.oct.mcd.remedy;

import javax.ejb.Local;
import br.com.ia.oct.mcd.common.monitoring.OCTComponent;
import br.com.ia.oct.mcd.scheduler.OCTJob;

@Local
public interface McdToRemedyProcessorLocal extends OCTJob, OCTComponent {

}
