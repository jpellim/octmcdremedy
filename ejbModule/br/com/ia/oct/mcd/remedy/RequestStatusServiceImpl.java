package br.com.ia.oct.mcd.remedy;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.entity.remedy.RequestStatus;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.infrastructure.trace.TraceService;

@Stateful
@Remote(RequestStatusService.class)
@Local(RequestStatusServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/requestStatusService")
@EJB(name = "octopus/mcd/remedy/requestStatusService", beanInterface = RequestStatusService.class)  
@OCTComponent(version="$LastChangedRevision$",uid="4.5.8", nome="RequestStatusServiceImpl", descricao="Componente para gerenciamento de status e status reason", jndiLookup="MCD/RequestStatusServiceImpl/local", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="REMEDY", ativo=true)
public class RequestStatusServiceImpl implements RequestStatusServiceLocal, RequestStatusService {
	private static final Logger LOG = Logger.getLogger(RequestStatusServiceImpl.class);
	private final String componentID = "4.5.8";
	//IA:PRJ_13467_16:Categ_Map
	private String requestStatusFormName = Configuration.getConfig("mcd.requestStatusService.statusFormName");
	
	//@EJB  //(mappedName="octopus/mcd/management/infrastructure/traceservice")
	@Inject
	private TraceService traceService;	
	
	//@EJB   //(mappedName="octopus/mcd/remedy/daoservice")
	@Inject
	private RemedyDAOService remedyDAOService;	
	
	public RequestStatus getStatusIN (String companyIN, String companyOUT, String statusIN) throws Exception {
		HashMap<Integer, String> fieldNames = remedyDAOService.getAllFieldNames(this.requestStatusFormName);
				
		String query = "'COMPANY_IN' = \"" + companyIN + "\"" +
						" AND 'COMPANY_OUT' = \"" + companyOUT + "\"" +
						" AND 'STATUS_IN' = \"" + statusIN + "\"";

		
		List<HashMap<String, String>> queryEntry = remedyDAOService.queryEntry(this.requestStatusFormName, query, fieldNames);
		
		traceService.trace(null, this.componentID, "Total de status para a query", "query=[" + query + "], total=" + queryEntry.size(), Calendar.getInstance());
		
		if (queryEntry.size() == 0) {
			return null;
		} else {
			HashMap<String, String> row = queryEntry.get(0);
			
			RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatusIN(row.get("STATUS_IN"));
			requestStatus.setStatusOUT(row.get("STATUS_OUT"));
			requestStatus.setCompanyIN(row.get("COMPANY_IN"));
			requestStatus.setCompanyOUT(row.get("COMPANY_OUT"));
			
			return requestStatus;			
		}
	}


	public RequestStatus getStatusOUT(String companyIN, String companyOUT, String statusOUT) throws Exception {
		HashMap<Integer, String> fieldNames = remedyDAOService.getAllFieldNames(this.requestStatusFormName);
		
		String query = "'COMPANY_IN' = \"" + companyIN + "\"" +
				" AND 'COMPANY_OUT' = \"" + companyOUT + "\"" +
				" AND 'STATUS_OUT' = \"" + statusOUT + "\"";
		
		List<HashMap<String, String>> queryEntry = remedyDAOService.queryEntry(this.requestStatusFormName, query, fieldNames);
		
		traceService.trace(null, this.componentID, "Total de status para a query", "query=[" + query + "], total=" + queryEntry.size(), Calendar.getInstance());
		
		if (queryEntry.size() == 0) {
			return null;
		} else {
			HashMap<String, String> row = queryEntry.get(0);
			
			RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatusIN(row.get("STATUS_IN"));
			requestStatus.setStatusOUT(row.get("STATUS_OUT"));
			requestStatus.setCompanyIN(row.get("COMPANY_IN"));
			requestStatus.setCompanyOUT(row.get("COMPANY_OUT"));
			
			return requestStatus;			
		}
	}
	
	public Boolean isAlive() {

		return true;
	}


	
}
