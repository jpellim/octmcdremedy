package br.com.ia.oct.mcd.remedy;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.bmc.arsys.api.Timestamp;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.utils.Configuration;

@Stateless
@Remote(RemedyGenerateCSV.class)
@Local(RemedyGenerateCSVBeanLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/generatecsvbean")
@EJB(name = "octopus/mcd/remedy/generatecsvbean", beanInterface = RemedyGenerateCSV.class) 
@OCTJob(uid="4.5.3", nome="RemedyGenerateCSVBean", descricao="JOB para criacao do csv de SITE e CMDB", jndiLookup="octopus/mcd/remedy/generatecsvbean", clazz="", cronTrigger="0 0 0/24 1/1 * ? *", grupo="REMEDY", ativo=true, node="node01")
@OCTComponent(version="$LastChangedRevision$",uid="4.5.3", nome="RemedyGenerateCSVBean", descricao="JOB para criacao do csv de SITE e CMDB", jndiLookup="MCD/RemedyGenerateCSVBean/local", clazz="", cronTrigger="0 0 0/24 1/1 * ? *", grupo="REMEDY", ativo=true)
@TransactionTimeout(value=3300) // 55 minutes
public class RemedyGenerateCSVBean implements RemedyGenerateCSV, RemedyGenerateCSVBeanLocal  {
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(RemedyGenerateCSVBean.class);
	private static boolean running  = false;
	SendSFTP sendSFTP = new SendSFTP();
	
//	@EJB  //(mappedName="octopus/mcd/remedy/boservice")
	@Inject
	private RemedyBOService remedyDAOService;
	
	public boolean execute(HashMap<String, Object> params) throws Exception {
		
		if (running) {
			LOG.info("Job anterior ainda em execução, saindo...");
			return true;
		}

		running     = true;
		LOG.info("Job - Inicio");
		
		if(this.getMcDonaldsSites() && this.getMcDonaldsCMDB()){
			LOG.info("Job executado com SUCESSO.");
		}else{
			LOG.error("Algum erro ocorreu ao criar os arquivos CSVs.");
		}
		
		LOG.info("Job finalizado.");
		running = false;
		return true;
	}
	
	public Boolean isAlive() {
		return true;
	}
	 
	/**
	 * Método usado para geração do arquivo CSV dos Sites cadastrados nas ultimas 24h.
	 * Configuração do local de armazenamento: oct-mcd.properties em (mcd.remedy.outputfile.Site)
	 * @return - Retorna um boolean afirmando se foi ou não concluido o procedimento
	 */
	public boolean getMcDonaldsSites(){
		
		String formName = Configuration.getConfig("mcd.remedy.formName.site");
		HashMap<Integer, String> fieldNames = new HashMap<Integer, String>();
		
		fieldNames.put(1000000001,"Company");
		fieldNames.put(260000001,"Site");
		fieldNames.put(1000000074,"Site ID");
		fieldNames.put(1000000081,"Status-STL");
		fieldNames.put(1000000030,"Local Site");
		fieldNames.put(1000000704,"Site Group Type");
		fieldNames.put(7,"Status-STC");
		fieldNames.put(200000007,"Site Group");
		fieldNames.put(1000000073,"Primary Alias");
		fieldNames.put(1000000078,"Site Alias");
		fieldNames.put(3,"Submit Date");
		fieldNames.put(1100,"Mode");
		fieldNames.put(1000000002,"Country");
		fieldNames.put(1000000004,"City");
		fieldNames.put(6,"Last Modified Date");
		 
		//Cria a query para extrair os Sites criados nas ultimas 24 horas.
		Calendar dt = Calendar.getInstance();
		Date data = dt.getTime();
		Timestamp  createDate = new Timestamp(data);
		String company = Configuration.getConfig("mcd.remedy.company");
		String query = "'Company' = \"" + company + "\" AND 'Last Modified Date' > \""+createDate.getValue()+"\" - 24*60*60";
		LOG.info("[getMcDonaldsSites] Buscando sites da company " + company + "(query: [" + query + "]");
		
		//Faz a extração de todos os Sites
		List<HashMap<String, String>> queryEntry = remedyDAOService.frmQuery(formName, query, fieldNames);
		StringBuffer retorno = new StringBuffer();
		
		if (queryEntry != null && queryEntry.size() > 0) {
			
			for (HashMap<String, String> row: queryEntry) {
				
				  retorno.append(row.get("Company") + ";");
				  retorno.append(row.get("Site") + ";");
				  retorno.append(row.get("Site ID") + ";");
				  retorno.append(row.get("Status-STL") + ";");
				  retorno.append(row.get("Local Site") + ";");
				  retorno.append(row.get("Site Group") + ";");
				  retorno.append(row.get("Status-STC") + ";");
				  retorno.append(row.get("Primary Alias") + ";");
				  retorno.append(row.get("Site Alias") + ";");
				  retorno.append(row.get("Submit Date") + ";");
				  retorno.append(row.get("Mode") + ";");
				  retorno.append(row.get("Country") + ";");
				  retorno.append(row.get("City") + ";");
				  retorno.append(row.get("Last Modified Date") + ";");
				  retorno.append(System.getProperty("line.separator"));
			}
		} else {
			retorno.append("SEM REGISTROS PARA EXIBIR\n");
		}
		
		try {
			//Faz a gravação do arquivo CSV no diretório apontado
			 File file = new File(Configuration.getConfig("mcd.remedy.outputfile.Site")); 
			 FileOutputStream out= new FileOutputStream(file,true); 
			 BufferedOutputStream bos = new BufferedOutputStream(out); 
			 bos.write(retorno.toString().getBytes()); 
			 bos.flush(); 
			 out.close(); 
			 bos.close(); 
			 LOG.info("[getMcDonaldsSites] Arquivo CSV criado com sucesso!!!");

			 try {
				//Envia o arquivo CSV para o Servidor SFTP
				 sendSFTP.send(Configuration.getConfig("mcd.remedy.outputfile.Site"));
				} catch (Exception e) {
					return false;
				}
			 
		} catch (Exception e) {
			 LOG.error("[getMcDonaldsSites] Erro ao criar o arquivo CSV, Mensagem: " + e.getMessage());
			 return false;
		}
		return true;	
	}

	/**
	 * Método usado para geração do arquivo CSV dos CMDBs cadastrados nas ultimas 24h.
	 * Configuração do local de armazenamento: oct-mcd.properties em (mcd.remedy.outputfile.CMDB)
	 * @return - Retorna um boolean afirmando se foi ou não concluido o procedimento.
	 * @throws Exception 
	 */
	public boolean getMcDonaldsCMDB() throws Exception{
		
		String formName = Configuration.getConfig("mcd.remedy.formName.cmdb");
		HashMap<Integer, String> fieldNames = remedyDAOService.getAllFieldNames(formName);
		
		//Cria a query para extrair os CMDBs criados nas ultimas 24 horas.
		Calendar dt = Calendar.getInstance();
		Date data = dt.getTime();
		Timestamp  createDate = new Timestamp(data);
		String company = Configuration.getConfig("mcd.remedy.company");
		String query = "'Company' = \"" + company + "\" AND 'ModifiedDate' > \""+createDate.getValue()+"\" - 24*60*60";
		LOG.info("[getMcDonaldsCMDB] Buscando CMDBs da company " + company + "(query: [" + query + "]");
		//Faz a extração de todos os CMDB
		List<HashMap<String, String>> queryEntry = remedyDAOService.frmQuery(formName, query, fieldNames);
		StringBuffer retorno = new StringBuffer();		

		if (queryEntry != null && queryEntry.size() > 0) {
			
			for (HashMap<String, String> row: queryEntry) {
				
				 retorno.append(row.get("Company") + ";");
				 retorno.append(row.get("Name") + ";");
				 retorno.append(row.get("Category") + ";");
				 retorno.append(row.get("Type") + ";");
				 retorno.append(row.get("Item") + ";");
				 retorno.append(row.get("Site") + ";");
				 retorno.append(row.get("Create Date") + ";");
				 retorno.append(row.get("Last Modified By") + ";");
				 retorno.append(System.getProperty("line.separator"));
			}
		} else {
			retorno.append("SEM REGISTROS PARA EXIBIR\n");
		}
		
		try {
			//Faz a gravação do arquivo CSV no diretório apontado
			 File file = new File(Configuration.getConfig("mcd.remedy.outputfile.CMDB")); 
			 FileOutputStream out= new FileOutputStream(file,true); 
			 BufferedOutputStream bos = new BufferedOutputStream(out); 
			 bos.write(retorno.toString().getBytes()); 
			 bos.flush(); 
			 out.close(); 
			 bos.close(); 
			 LOG.info("[getMcDonaldsCMDB] Arquivo CSV criado com sucesso!!!");
			 
			 try {
				 //Envia o arquivo CSV para o Servidor SFTP
				 sendSFTP.send(Configuration.getConfig("mcd.remedy.outputfile.CMDB"));
			} catch (Exception e) {
				LOG.error(e.getMessage());
				return false;
			}
			 
		} catch (Exception e) {
			 LOG.error("[getMcDonaldsCMDB] Erro ao criar o arquivo CSV, Mensagem: " + e.getMessage());
			 return false;
		}
		return true;
	}
}
