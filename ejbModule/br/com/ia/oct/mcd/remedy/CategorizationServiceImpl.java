package br.com.ia.oct.mcd.remedy;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.entity.McDMapping;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.dataservice.DataService;
import br.com.ia.oct.mcd.infrastructure.trace.TraceService;

@Stateful
@Remote(CategorizationService.class)
@Local(CategorizationServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/categorizationservice")
@EJB(name = "octopus/mcd/remedy/categorizationservice", beanInterface = CategorizationService.class) 
@OCTComponent(version="$LastChangedRevision$",uid="4.5.9", nome="CategorizationServiceImpl", descricao="Componente para gerenciamento de categorizações", jndiLookup="octopus/mcd/remedy/categorizationservice", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="REMEDY", ativo=true)
public class CategorizationServiceImpl implements CategorizationServiceLocal, CategorizationService{
	private static final Logger LOG = Logger.getLogger(CategorizationServiceImpl.class);
	private final String componentID = "4.5.9";
	
	private String tblMcDMappings = "IA_MCDONALDS_V01_MAPEAMENTO_AT";
	
	
	//@EJB //(mappedName="octopus/mcd/management/infrastructure/traceservice")
	@Inject
	private TraceService traceService;
	
	//@EJB  //(mappedName="octopus/mcd/management/dataservice")
	@Inject
	private DataService<McDMapping> mapService; 

//	@EJB  //(mappedName="octopus/mcd/remedy/daoservice")
	@Inject
	private RemedyDAOService remedyDAOService;	

	@PostConstruct
	public void inicializa () {
		OCTUtils.setProxyConfig();
	}

	public String[] getMap(String p_tipo_req, String p_map, String p_sentido, String p_de_1, String p_de_2, String p_de_3, String p_de_4) {
				
		String[] ret   = new String[5];
		String qryB    = " select Categoria, ccti_item, ccti_type, Last_Name2, do_integration from " + tblMcDMappings + " where Tipo_de_categorizacao = " + p_map + " AND Status = 0";
		String qryC    = " select Categoria, ccti_item, ccti_type, Last_Name2, do_integration from " + tblMcDMappings + " where Tipo_de_categorizacao = " + p_map + " AND Status = 0 and Tipo_de_incidente = 'Default' ";
		List<HashMap<String, String>> recs = null;

		if (p_de_1 != null && p_de_1.trim().length() > 0) {
			qryB += " AND Tier_1 = '" + p_de_1.trim() + "'";  
		}

		if (p_de_2 != null && p_de_2.trim().length() > 0) {
			qryB += " AND Tier_2 = '" + p_de_2.trim() + "'";  
		}

		if (p_de_3 != null && p_de_3.trim().length() > 0) {
			qryB += " AND Tier_3 = '" + p_de_3.trim() + "'";  
		}

		if (p_de_4 != null && p_de_4.trim().length() > 0) {
			qryB += " AND First_Name = '" + p_de_4.trim() + "'";  
		}

		LOG.info("Buscando mapeamento (" + qryB + ")...");
		recs = mapService.executeQuery(qryB);

		if (recs != null && recs.size() > 0) {
			
			Iterator itrB = recs.iterator();
			
			while(itrB.hasNext()){
				Object[] obj = (Object[]) itrB.next();
				ret[0] = String.valueOf(obj[0]); 
				ret[1] = String.valueOf(obj[1]);			
				ret[2] = String.valueOf(obj[2]);
				ret[3] = String.valueOf(obj[3]);
				ret[4] = String.valueOf(obj[4]);
			 }
		
		} else {
			LOG.warn("Mapeamento requerido nao encontrado, buscando mapeamento default (" + p_map + ")");
			LOG.info("Buscando mapeamento (" + qryC + ")...");
			recs = mapService.executeQuery(qryC);

			if (recs != null && recs.size() > 0) {
				Iterator itrC = recs.iterator();
				
				while(itrC.hasNext()){
					Object[] obj = (Object[]) itrC.next();
					ret[0] = String.valueOf(obj[0]); 
					ret[1] = String.valueOf(obj[1]);
					ret[2] = String.valueOf(obj[2]);
					ret[3] = String.valueOf(obj[3]);
					ret[4] = String.valueOf(obj[4]);
				}
				
				LOG.info("Mapeamento default encontrado (" + p_map + ")!");				
			} else {
				LOG.warn("Mapeamento default não encontrado (" + p_map + ")!");
				ret[0] = "N/A#1";
				ret[1] = "N/A#2";
				ret[2] = "N/A#3";
				ret[3] = "N/A#4";
			}
		}

		LOG.info("Mapeamento encontrado: " + ret[0] + " / " + ret[1] + " / " + ret[2] + " / " + ret[3]);
		return ret;
				
	}

	
	public String[] getMapMcD (String p_map, String p_para_1, String p_para_2, String p_para_3, String p_para_4, Boolean pBuscarDefault){
		
		String[] ret   = new String[4];
		List<HashMap<String, String>> recs = null;
		
		String qry = "Select Tier_1, Tier_2, Tier_3, do_integration from " + tblMcDMappings + " where Tipo_de_Categorizacao = " + p_map + " and Status = 0 ";
		
		if ((p_para_1 != null) && (p_para_1.trim().length() > 0)){			
			qry += " and Categoria = '" + p_para_1 + "'"; 
		}
		
		if ((p_para_2 != null) && (p_para_2.trim().length() > 0)){			
			qry += " and ccti_item = '" + p_para_2 + "'"; 
		}
		
		LOG.info("Buscando mapeamento (" + qry + ")...");
		recs = mapService.executeQuery(qry);

		if (recs != null && recs.size() > 0) {
			Iterator itrB = recs.iterator();
			
			while(itrB.hasNext()){
				Object[] obj = (Object[]) itrB.next();
				ret[0] = String.valueOf(obj[0]); 
				ret[1] = String.valueOf(obj[1]);			
				ret[2] = String.valueOf(obj[2]);
				ret[3] = String.valueOf(obj[3]);
			 }
		} else {
			if (pBuscarDefault) {
				
				String qryDefault = " select tier_1, tier_2, tier_3, do_integration from " + tblMcDMappings + " where Tipo_de_Incidente = 'Default' ";
				
				LOG.warn("Mapeamento requerido nao encontrado, buscando mapeamento default (" + p_map + ", "+ p_para_1 + "," + p_para_2 + "," + p_para_3 + "," + p_para_4 + ")");							
				LOG.info("Buscando mapeamento (" + qryDefault + ")...");
				
				recs = mapService.executeQuery(qryDefault);
				
				if (recs != null && recs.size() > 0) {
					Iterator itrB = recs.iterator();
					
					while(itrB.hasNext()){
						Object[] obj = (Object[]) itrB.next();
						ret[0] = String.valueOf(obj[0]); 
						ret[1] = String.valueOf(obj[1]);			
						ret[2] = String.valueOf(obj[2]);
						ret[3] = String.valueOf(obj[3]);
					 }
					
					LOG.info("Mapeamento default encontrado (" + p_map + ")!");			
					
				} else {
					LOG.warn("Mapeamento default não encontrado (" + p_map + ")!");
				}										
			}
			
			LOG.warn("Mapeamento não encontrado");
		}
		
		return ret;
	}
	

	@Override
	public Boolean isAlive() {

		return true;
	}

}
