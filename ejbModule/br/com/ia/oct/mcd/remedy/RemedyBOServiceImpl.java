package br.com.ia.oct.mcd.remedy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import org.apache.log4j.Logger;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.ARSetGetEntryException;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.DataType;
import com.bmc.arsys.api.DateTimeField;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.entity.remedy.RemedyAttachment;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.OCTUtils;

@Stateful
@Remote(RemedyBOService.class)
@Local(RemedyBOServiceLocal.class)
//@EJB(name = "java:global/octopus/mcd/remedy/boservice", beanInterface = RemedyBOService.class)  
@EJB(name = "java:global/OCT-McD/OCT-MCD-Remedy/RemedyBOServiceImpl", beanInterface = RemedyBOService.class)  
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/boservice")
@OCTComponent(version="$LastChangedRevision$",uid="4.5.7", nome="RemedyBOServiceImpl", descricao="Componente de regras de negócio do Remedy", jndiLookup="MCD/RemedyBOServiceImpl/local", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="REMEDY", ativo=true)
public class RemedyBOServiceImpl implements RemedyBOService, RemedyBOServiceLocal {
	private static final Logger LOG = Logger.getLogger(RemedyBOServiceImpl.class);
	private static ARServerUser ars;
	private static String REMEDY_USER 	= "";
	private static String REMEDY_PASSWD 	= "";
	private static String REMEDY_SERVER 	= "";
	private static int 	  REMEDY_PORT 	= 0;
	
	private static String REMEDY_SERVER2 = "";
	private static int    REMEDY_PORT2 = 0;
	
	@PostConstruct
	public void inicializa () {
		REMEDY_USER   = Configuration.getConfig("mcd.remedy.params.username");
		REMEDY_PASSWD = Configuration.getConfig("mcd.remedy.params.password");
		REMEDY_SERVER = Configuration.getConfig("mcd.remedy.params.server");
		REMEDY_PORT   = Integer.parseInt(Configuration.getConfig("mcd.remedy.params.port"));
						
		
		try{
			if (Configuration.getConfig("mcd.remedy.params.server2") != null) {
				REMEDY_SERVER2 = Configuration.getConfig("mcd.remedy.params.server2");
				
				REMEDY_PORT2 = REMEDY_PORT;
				if (Configuration.getConfig("mcd.remedy.params.port2") != null) {
					REMEDY_PORT2 = Integer.parseInt(Configuration.getConfig("mcd.remedy.params.port2"));	
				} 
			}
		}catch (Exception e) {
			LOG.info("Server2 ou porta2 não encontrados nos parâmetros de configuração.");			
		}
				
		
		OCTUtils.setProxyConfig();
		LOG.info("Utilizando parâmetros de inicialização [REMEDY_USER=" + REMEDY_USER + ", REMEDY_PASSWD=" + REMEDY_PASSWD + ", REMEDY_SERVER=" + REMEDY_SERVER + ", REMEDY_PORT=" + REMEDY_PORT + ", REMEDY_SERVER2=" + REMEDY_SERVER2 + ", REMEDY_PORT2=" + REMEDY_PORT2 + "]");
	}

	public String sayHello() {
		return "Hello from RemedyBOService!";
	}
	
	@PreDestroy
	public void finaliza () {
		if (ars != null) {
			String userSessionGuid = ars.getUserSessionGuid();
			
			if (userSessionGuid != null) {
				LOG.info("Efetuando logout na plataforma Remedy.");
				ars.logout();	
				ars = null;
			}
		}
	}

	public String createIncident(String summary, String notes, String company, String tier1, 
			String tier2, String tier3, String impact, String urgency, String statusOUT, String serviceType, String number) throws Exception {
		this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
		String createEntry = null;
		String incidentId  = null;
		Entry e            = new Entry();
		e.put(7,          new Value(statusOUT));            // Status
		e.put(8,          new Value("INT_MCDONALDS"));      // Short Description
		e.put(1000000000, new Value(summary));              // Description/Summary
		e.put(1000000018, new Value(Configuration.getConfig("mcd.remedy.lastname")));  // Last name "" (FN), ""
		e.put(1000000019, new Value(Configuration.getConfig("mcd.remedy.firstname"))); // First name
		e.put(1000000063, new Value(tier1));                // Operational Categorization tier 1
		e.put(1000000064, new Value(tier2));                // Operational Categorization tier 2
		e.put(1000000065, new Value(tier3));                // Operational Categorization tier 3
		e.put(1000000076, new Value("CREATE"));             // z1D_Action
		e.put(1000000099, new Value(serviceType));          // Service type
		e.put(1000000151, new Value(notes));                // Details/Notes
		e.put(1000000215, new Value("3000"));               // Reported source

		if (urgency == null || urgency.isEmpty()) {
			e.put(1000000162, new Value("3000"));
		} else {
			e.put(1000000162, new Value(urgency));
		}

		if (impact == null || impact.isEmpty()) {
			e.put(1000000163, new Value("3000"));
		} else {
			e.put(1000000163, new Value(impact));
		}

		e.put(1000000652, new Value(number));  // Vendor Ticket Number

		try {
			int frm_incno_fieldID = 1000000161;
			createEntry = ars.createEntry("HPD:IncidentInterface_Create", e);
			int[] iic_fieldID = {1000000161};
			Entry e1 = remedySearch("HPD:IncidentInterface_Create", createEntry, iic_fieldID);

			if (e1 != null) {
				incidentId = e1.get(frm_incno_fieldID).getValue().toString();
			}

		} catch (ARSetGetEntryException arSetGetEntryException){
			LOG.error("Exception ao criar incidente no Remedy. Mensagem 1: " + arSetGetEntryException.getMessage());
		} catch (ARException arException){
			LOG.error("Exception ao criar incidente no Remedy. Mensagem 2: " + arException.getMessage());
		} catch (Exception exx) {
			LOG.error("Exception ao criar incidente no Remedy. Mensagem 3: " + exx.getMessage());
		} finally {
			this.logout();
		}
		
		return incidentId;
	}	

	public String createWorkInfo(String Notes, String incidentID) throws Exception {
		this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
		
		String registro = null;

		Entry e = new Entry();
		e.put(1000000161, new Value(incidentID));
		e.put(1000000151, new Value(Notes));
		e.put(1000000170, new Value("Customer Communication"));
		e.put(1000000000, new Value("Comunicação do Cliente"));
		
		try {
			registro = ars.createEntry("HPD:WorkLog", e);
		} catch (ARException e1) {
			throw new Exception(e1.getMessage(), e1);
		} finally {
			this.logout();	
		}
		
		return registro;
	}	
	
	private Entry remedySearch(String formName, String requestID, int[] fieldId) throws Exception {
		Entry registro = null;
		
		try {
			registro = ars.getEntry(formName, requestID, fieldId);
		} catch (Exception e) {
			throw e;
		}

		return registro;
	}	
	
	private void login(String user, String passwd, String server, int port, int rpc) throws Exception {
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.setPort(port);
				_ars.usePrivateRpcQueue(rpc);
				_ars.login();
				ars = _ars;
			}			
			// LOG.info("Login Remedy Realizado com Sucesso!!");
		} catch(ARException e){
			LOG.error("Erro no login!!! " + e.getMessage());
			throw new EJBException(e.getMessage());
		}
	}
	
	private void login(String user, String passwd, String server, int port) throws Exception {		
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.setPort(port);
				_ars.login();
				ars = _ars;
			}
			// LOG.info("Login Remedy Realizado com Sucesso!! ");
		} catch(ARException e){
			
			LOG.error("Erro no login!!! " + e.getMessage());
			
			try {
				if (!REMEDY_SERVER2.isEmpty()) {
					ARServerUser _ars2 = new ARServerUser();
					_ars2.setUser(user);
					_ars2.setPassword(passwd);
					_ars2.setServer(REMEDY_SERVER2);
					_ars2.setPort(REMEDY_PORT2);
					_ars2.login();
					ars = _ars2;
					
				}
			} catch (ARException e2) { 
				LOG.error("Erro no login server2!!!" + e.getMessage());
				throw new EJBException(e.getMessage());
			}			
						
			if (ars == null) {
				throw new EJBException(e.getMessage());
			}
		}
	}
	
	private void login(String user, String passwd, String server) throws Exception {
		try {
			if (ars == null) {
				ARServerUser _ars = new ARServerUser();
				_ars.setUser(user);
				_ars.setPassword(passwd);
				_ars.setServer(server);
				_ars.login();
				ars = _ars;
			}
			// LOG.info("Login Remedy Realizado com Sucesso!! ");
		} catch(ARException e){	
			LOG.error("Erro no login!!! " + e.getMessage());
			throw new EJBException(e.getMessage());
		}
	}

	private void logout() {
		ars.logout();
	}	

	public Boolean isAlive() {
		return true;
	}

	public String createEntry(String formName, HashMap<Integer, Object> object) throws Exception {
		return this.createRegistro(formName, object);
	}

	private String createRegistro(String formName, HashMap<Integer, Object> valores) throws Exception {
		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			Entry entry = new Entry();
			Set<java.util.Map.Entry<Integer, Object>> entrySet = valores.entrySet();
			Iterator<java.util.Map.Entry<Integer, Object>> iterator = entrySet.iterator();
			while(iterator.hasNext()){
				java.util.Map.Entry<Integer, Object> next = iterator.next();
				if(next.getValue() instanceof Integer){
					Integer obj = (Integer) next.getValue();
					entry.put(next.getKey(), new Value(obj));
				} else if (next.getValue() instanceof String){
					String obj = (String) next.getValue();
					entry.put(next.getKey(), new Value(obj));
				} else if (next.getValue() instanceof Calendar){
					Calendar obj = (Calendar) next.getValue();
					entry.put(next.getKey(), new Value(new Timestamp(obj.getTime())));
				} else if(next.getValue() instanceof RemedyAttachment) {
					RemedyAttachment att = (RemedyAttachment) next.getValue();
					AttachmentValue v = new AttachmentValue(att.getByteArray());
					v.setName(att.getName());
					entry.put(next.getKey(), new Value(v));
				}
			}

			String createEntry = ars.createEntry(formName, entry);
			this.logout();
			LOG.debug("Registro Remedy: " + createEntry);
			return createEntry;
		} catch (ARException e) {
			LOG.error("ARException", e);
			throw new EJBException(e.getMessage());
		} catch (Exception e) {
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}

	public List<HashMap<String, String>> frmQuery(String p_frm, String p_qry) {
		List<HashMap<String, String>> ret = null;
		HashMap<Integer, String> fields = null;
		
		LOG.info("Getting fields...");
		try {
			fields = this.getAllFieldNames(p_frm);
			LOG.info("Fields on memory...");
		} catch (Exception e) {
			LOG.error("Impossivel recuperar definição de campos do form [" + p_frm + "]. Mensagem: " + e.getMessage());
		}

		if (fields != null) {
			try {
				LOG.info("Doing query.");
				LOG.info("Query being done: [" + p_qry + "]");
				ret = this.queryEntry(p_frm, p_qry, fields);
				LOG.info("Query done.");
			} catch (Exception e) {
				LOG.error("Impossivel realizar a query [" + p_qry + "] no form [" + p_frm + "]. Mensagem: " + e.getMessage());
			}	
		}
		return ret;
	}
	
	public List<HashMap<String, String>> frmQuery(String p_frm, String p_qry, HashMap<Integer, String> fields){
		List<HashMap<String, String>> ret = null;

		if (fields != null) {
			try {
				ret = this.queryEntry(p_frm, p_qry, fields);
			} catch (Exception e) {
				LOG.error("Impossivel realizar a query [" + p_qry + "] no form [" + p_frm + "]. Mensagem: " + e.getMessage());
			}	
		}
		return ret;
	}

	public HashMap<Integer, String> getAllFieldNames(String formName) throws Exception {
		try {
			HashMap<Integer, String> fieldReturn = new HashMap<Integer, String>();
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			List<Field> listFieldObjects = ars.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA);
			for(Field f : listFieldObjects){
				if(f.getFieldID() != 15)
					fieldReturn.put(f.getFieldID(),f.getName());
			}
			
			this.logout();
			return fieldReturn;
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}
	}

	public List<HashMap<String, String>> queryEntry(String formName, String query, HashMap<Integer, String> campos) throws Exception {
		try {
			LOG.info("Internal query ini");
			
			LOG.info("Login remedy ini");
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			LOG.info("Login remedy end");
			
			int[] fieldId = new int[campos.size()];
			Iterator<Integer> iterator = campos.keySet().iterator();
			int a=0;

			while(iterator.hasNext()){
				fieldId[a] = iterator.next();
				a++;
			}
			
			List<Entry> listEntryObjects = ars.getListEntryObjects(formName, ars.parseQualification(formName,query),0,1000,null,fieldId,false,null);
			List<Field> getListF = ars.getListFieldObjects(formName,fieldId);
			List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();

			for(Entry e : listEntryObjects){
				HashMap<String, String> r = new HashMap<String, String>();
				for(Field f: getListF){
					Value v = e.get(f.getFieldID());
					if(v.getValue() == null){
						r.put(f.getName(), null);
						LOG.debug("Field: " + f.getName() + " - Value: " + null);
					} else {
						if(v.getDataType().equals(DataType.CURRENCY)){
							r.put(f.getName(), v.getCurrencyValue());
							LOG.debug("Field: " + f.getName() + " - Value: " + v.getCurrencyValue());
						} else if(v.getDataType().equals(DataType.INTEGER)){
							r.put(f.getName(), Integer.toString(v.getIntValue()));
							LOG.debug("Field: " + f.getName() + " - Value: " + v.getIntValue());
						} else if (f instanceof DateTimeField) {
							SimpleDateFormat df_bmc = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.bmc"));
							SimpleDateFormat df_out = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.out"));							
							Timestamp tsValue = (Timestamp) v.getValue();
							Date date_value   = df_bmc.parse(tsValue.toDate().toString()); 
							r.put(f.getName(), df_out.format(date_value));
						} else {
							r.put(f.getName(), v.getValue().toString());
							LOG.debug("Field: " + f.getName() + " - Value: " + v.getValue().toString());
						}
					}
				}

				retorno.add(r);			
			}

			LOG.info("Internal query end - " + retorno.size());
			
			this.logout();
			return retorno;
		} catch(ARException ee){
			LOG.error("Exception", ee);
			throw new EJBException(ee.getMessage());
			
		}catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}	
		
	}

	private String buildDebugMsg(Integer p_level, String p_msg) {
		String ret = "";
		String classname = this.getClass().getSimpleName();

		switch (p_level) {
			case 1:
				ret = "[MCD][" + classname + "][WARN]: " + p_msg; 
				break;
			case 2:
				ret = "[MCD][" + classname + "][ERRO]: " + p_msg; 
				break;
			default:
				ret = "[MCD][" + classname + "][INFO]: " + p_msg; 
				break;
		}

		return ret;
	}
	
	private void printDebugMsg(Integer p_level, String p_msg) {
		System.out.println(buildDebugMsg(p_level, p_msg));
	}

	public boolean updateEntry(String formName, String requestId, HashMap<String, Object> object) throws Exception {
		boolean ret = false;

		try {
			this.login(REMEDY_USER, REMEDY_PASSWD, REMEDY_SERVER, REMEDY_PORT);
			Entry e = new Entry();
			List<Field> listFieldObjects = ars.getListFieldObjects(formName);

			for(Field f : listFieldObjects){
				if(object.containsKey(f.getName())){
					if(object.get(f.getName()) instanceof Integer){
						Integer obj = (Integer) object.get(f.getName());
						e.put(f.getFieldID(), new Value(obj) );
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					} else if (object.get(f.getName()) instanceof String){
						String obj = (String) object.get(f.getName());
						e.put(f.getFieldID(), new Value(obj) );
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					} else if (object.get(f.getName()) instanceof Calendar){
						Calendar obj = (Calendar) object.get(f.getName());
						e.put(f.getFieldID(), new Value(new Timestamp(obj.getTime())));
						System.out.println("** Campo a ser atualizado: " + f.getFieldID() + " com o valor: " + obj);
					}
				}
			}

			ars.setEntry(formName, requestId, e, null, 0);
			this.logout();
			ret = true;
		} catch (ARException arEx){
			LOG.error(arEx);			
		} catch(Exception e){
			LOG.error("Exception", e);
			throw new EJBException(e.getMessage());
		}

		return ret;
	}

}
