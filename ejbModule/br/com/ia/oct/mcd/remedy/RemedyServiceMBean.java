package br.com.ia.oct.mcd.remedy;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.ia.oct.mcd.common.utils.Configuration;

/**
 * Serviço JMX utilizado para criação dos arquivos CSVs de Site e CMDB 
 * @author Vinicius Silva
 *
 */
@Singleton //(name="MCD.Remedy:service=RemedyManager")
@Startup //(RemedyService.class)
public class RemedyServiceMBean implements RemedyService {
	private static final Logger LOG = Logger.getLogger(RemedyServiceMBean.class);
	SendSFTP sendSFTP = new SendSFTP();
	//@EJB  //(mappedName="octopus/mcd/remedy/boservice")
	
	@Inject
	private RemedyBOService remedyDAOService;
	
	public void start() throws Exception {

	}

	public void destroy() throws Exception {

	}
	
	/**
	 * Método usado para geração do arquivo CSV full do site do MCD.
	 * Configuração do local de armazenamento: oct-mcd.properties em (mcd.remedy.outputfile.allSite)
	 * @param - Nome da company do MCD. Company utilizada para teste: ARCOS DOURADOS - DC
	 * @return - Retorna uma mensagem na JMX referente ao procedimento executado
	 */
	public String getAllMcDonaldsSites(String companyName) throws Exception {
		
		String formName = Configuration.getConfig("mcd.remedy.formName.site");
		HashMap<Integer, String> fieldNames =remedyDAOService.getAllFieldNames(formName);
		String [] array = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		LOG.info("[getAllMcDonaldsSites] Buscando sites da company: " + companyName + "");
		 for(int i=0;i < array.length;i++){
			 
			boolean reWrite = true;
			 
			if(i==0){
				reWrite = false;
			}
			
			String query = "'Company' = \"" + companyName + "\" AND 'Site' LIKE \""+array[i]+"%\"";
			//LOG.info("[getAllMcDonaldsSites] Buscando sites da company: " + companyName + " (Query: [" + query + "]");
			//Faz a extração de todos os Sites
			List<HashMap<String, String>> queryEntry = remedyDAOService.frmQuery(formName, query,fieldNames);
			StringBuffer retorno = new StringBuffer();
	
			if (queryEntry != null && queryEntry.size() > 0) {
				 
				retorno.append("COMPANY;SITE;SITE ID;STATUS-STL;LOCAL SITE;"+
						  "SITE GROUP TYPE;STATUS-STC;SITE GROUP;PRIMARY ALIAS;SITE ALIAS;SUBMIT DATE;"+
						 "MODE;COUNTRY;CITY;LAST MODIFIED DATE;" + "\n");
	
				for (HashMap<String, String> row: queryEntry) {
					  retorno.append(row.get("Company") + ";");
					  retorno.append(row.get("Site") + ";");
					  retorno.append(row.get("Site ID") + ";");
					  retorno.append(row.get("Status-STL") + ";");
					  retorno.append(row.get("Local Site") + ";");
					  retorno.append(row.get("Site Group") + ";");
					  retorno.append(row.get("Status-STC") + ";");
					  retorno.append(row.get("Primary Alias") + ";");
					  retorno.append(row.get("Site Alias") + ";");
					  retorno.append(row.get("Submit Date") + ";");
					  retorno.append(row.get("Mode") + ";");
					  retorno.append(row.get("Country") + ";");
					  retorno.append(row.get("City") + ";");
					  retorno.append(row.get("Last Modified Date") + ";");
					  retorno.append(System.getProperty("line.separator"));
				}
				try {
					//Faz a gravação do arquivo CSV no diretório apontado
					 File file = new File(Configuration.getConfig("mcd.remedy.outputfile.allSite")); 
					 FileOutputStream out= new FileOutputStream(file,reWrite); 
					 BufferedOutputStream bos = new BufferedOutputStream(out); 
					 bos.write(retorno.toString().getBytes()); 
					 bos.flush(); 
					 out.close(); 
					 bos.close(); 
					
				} catch (Exception e) {
					 LOG.error("[getAllMcDonaldsSites] Erro ao criar o arquivo CSV, Mensagem: " + e.getMessage());
					 return ("Ocorreu um erro ao criar o arquivo CSV do Site, Mensagem: " + e.getMessage());
				}

			} 
		}
		 	LOG.info("[getAllMcDonaldsSites] Arquivo CSV criado com sucesso.");
		  
		 	try {
		 	 //Envia o arquivo CSV para o Servidor SFTP
			 sendSFTP.send(Configuration.getConfig("mcd.remedy.outputfile.allSite"));
			 } catch (Exception e) {
				return ("Ocorreu um erro ao enviar o arquivo CSV para o servidor SFTP, Mensagem: " + e.getMessage());
			}
		  
	return ("Extração concluida!!!");
	
}
	
	/**
	 * Método usado para geração do arquivo CSV full do CMDB do MCD.
	 * Configuração do local de armazenamento: oct-mcd.properties em (mcd.remedy.outputfile.allCMDB)
	 * @param - Nome da company do MCD. Company utilizada para teste: ARCOS DOURADOS - DC
	 * @return - Retorna uma mensagem na JMX referente ao procedimento executado
	 */
	public String getAllMcDonaldsCMDB(String companyName) throws Exception {
		
		String formName = Configuration.getConfig("mcd.remedy.formName.cmdb");
		HashMap<Integer, String> fieldNames = remedyDAOService.getAllFieldNames(formName);
		LOG.info("Buscando CMDB da company: " + companyName + "");
		String query = "'Company' = \"" + companyName + "\"";
		//LOG.info("(Query: [" + query + "]");
		
		//Faz a extração de todos os CMDBs
		List<HashMap<String, String>> queryEntry = remedyDAOService.frmQuery(formName, query, fieldNames);
		StringBuffer retorno = new StringBuffer();
			
			if (queryEntry != null && queryEntry.size() > 0) {
				
				retorno.append("COMPANY;NAME;TIER1;TIER2;TIER3;SITE;CREATE DATE;LAST MODIFIED BY;" + "\n");
				
				for (HashMap<String, String> row: queryEntry) {
					
					 retorno.append(row.get("Company") + ";");
					 retorno.append(row.get("Name") + ";");
					 retorno.append(row.get("Category") + ";");
					 retorno.append(row.get("Type") + ";");
					 retorno.append(row.get("Item") + ";");
					 retorno.append(row.get("Site") + ";");
					 retorno.append(row.get("Create Date") + ";");
					 retorno.append(row.get("Last Modified By") + ";");
					 retorno.append(System.getProperty("line.separator"));
				}
				
				try {
					//Faz a gravação do arquivo CSV no diretório apontado
					 File file = new File(Configuration.getConfig("mcd.remedy.outputfile.allCMDB")); 
					 FileOutputStream out= new FileOutputStream(file,false); 
					 BufferedOutputStream bos = new BufferedOutputStream(out); 
					 bos.write(retorno.toString().getBytes()); 
					 bos.flush(); 
					 out.close(); 
					 bos.close();
					 LOG.info("[getAllMcDonaldsCMDB] Arquivo CSV criado com sucesso.");	
					 
					 try {
						//Envia o arquivo CSV para o Servidor SFTP
						 sendSFTP.send(Configuration.getConfig("mcd.remedy.outputfile.allCMDB"));
						 } catch (Exception e) {
							return ("Ocorreu um erro ao enviar o arquivo CSV do CMDB para o servidor SFTP, Mensagem: " + e.getMessage());
						}
					 
				} catch (Exception e) {
					 LOG.error("[getAllMcDonaldsCMDB] Erro ao criar o arquivo CSV do CMDB, Mensagem: " + e.getMessage());
					 return ("Ocorreu um erro ao criar o arquivo CSV, Mensagem: " + e.getMessage());
				}
			} 
	   return ("Extração concluida!!!");
	}	
}