package br.com.ia.oct.mcd.remedy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.inteqnet.core.appservices.webservice.beans.xsd.Incident;
import com.inteqnet.core.appservices.webservice.beans.xsd.Worklog;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.entity.McDMappingTipoMap;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.MsgBarramentoEnum;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.itmass.ITMassAPI;
import br.com.ia.oct.mcd.itmass.IncidentOperations;
import net.sf.json.JSONObject;


/**
 * Session Bean implementation class McdToRemedyProcessorBean
 */
@Stateless
@Remote(McdToRemedyProcessor.class)
@Local(McdToRemedyProcessorLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/mcdtoremedy")
@EJB(name = "octopus/mcd/remedy/mcdtoremedy", beanInterface = McdToRemedyProcessor.class)  
@OCTJob(uid="4.5.1", nome="McdToRemedyProcessorBean", descricao="JOB para processamento de registros do form intermediario", jndiLookup="octopus/mcd/remedy/mcdtoremedy", clazz="", cronTrigger="0 0/30 * 1/1 * ? *", grupo="REMEDY", ativo=false, node="node01")
@OCTComponent(version="$LastChangedRevision$",uid="4.5.1", nome="McdToRemedyProcessorBean", descricao="JOB para processamento de registros do form intermediario", jndiLookup="MCD/McdToRemedyProcessorBean/local", clazz="", cronTrigger="0 0/1 * 1/1 * ? *", grupo="REMEDY", ativo=false)
@TransactionTimeout(value=3300) // 55 minutes
public class McdToRemedyProcessorBean implements McdToRemedyProcessor, McdToRemedyProcessorLocal {
	private static final long serialVersionUID = 1L;
	private static boolean running  = false;
	private static final Logger LOG = Logger.getLogger(McdToRemedyProcessorBean.class);
	private ITMassAPI acessAPI;

//	@EJB  //(mappedName="octopus/mcd/remedy/boservice")
    @Inject
	private RemedyBOService remedyBO;

//	@EJB //(mappedName="octopus/mcd/itmass/incidentops")
    @Inject
	private IncidentOperations ITMass;

//	@EJB  //(mappedName="octopus/mcd/remedy/categorizationservice")
    @Inject
	private CategorizationService categsrv;
	
	public Boolean isAlive() {
		return true;
	}

	@PostConstruct
	public void inicializa () {
		OCTUtils.setProxyConfig();
		acessAPI = new ITMassAPI();
		acessAPI.init();
	}

	public boolean execute(HashMap<String, Object> params) throws Exception {
		if (running) {
			LOG.info("Job anterior ainda em execução, saindo...");
			return true;
		}

		running = true;
		LOG.info("Executando job.");
		List<HashMap<String, String>> listaIncidentesNovos = null;
		// pesquisa novos incidentes no form intermediário.
		listaIncidentesNovos = this.getNewIncidents();

		if (listaIncidentesNovos != null) {
			if (listaIncidentesNovos.size() > 0) {
				for (HashMap<String, String> reqnew : listaIncidentesNovos) {
					LOG.info("Processando MCD#" + reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID"));

					if (createOrUpdateIncident(reqnew)) {
						LOG.info("MCD#" + reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID") + " processado com sucesso.");
					} else {
						LOG.warn("Erro ao processar MCD#" + reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID"));
					}
				}

			} else {
				LOG.warn("A busca por requests com status NOVO retornou 0 (zero) registros.");
			}
		} else {
			LOG.warn("A busca por requests com status NOVO retornou nulo.");
		}
		
		LOG.info("Job finalizado.");
		running = false;
		return true;
	}

	/**
	 * Cria ou atualiza um chamado na HPD:HelpDesk
	 * @param reqnew Parâmetros do incidente
	 * @return Verificação da operação no tipo boolean
	 */
	private boolean createOrUpdateIncident(HashMap<String, String> reqnew) {
		boolean ret = false;
		boolean cal = false;
		List<HashMap<String, String>> inc_hpdhd = null;
		String inc_mcd = reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID");
		String inc_tlf = null;

		if (inc_mcd != null && inc_mcd.trim().length() > 0) {
				LOG.info("Buscando incidente McDonalds [" + inc_mcd + "] no Remedy...");
				
				HashMap<Integer, String> fields = new HashMap<Integer, String>();
				fields.put(1000000161, "Incident Number");
				fields.put(6, "Last Modified Date");
				fields.put(1, "Entry ID");				
				
				inc_hpdhd = remedyBO.frmQuery("HPD:Help Desk", "'Vendor Ticket Number' = \"" + inc_mcd + "\""); 

				if (inc_hpdhd != null) {
					
					try {

						JSONObject mcd_data = ITMass.getIncident(inc_mcd);							
						
						String[] inc_cop = this.validaCategoriaOperacional(mcd_data, inc_mcd);					
						
						
						if (inc_hpdhd.size() > 0) {
							inc_tlf = inc_hpdhd.get(0).get("Incident Number");

													
							if (inc_cop == null) {
								this.atualizaFrmIntermediario(reqnew.get("Request ID"), MsgBarramentoEnum.CATEGORIA.getDescricaoErro(), inc_tlf, 2);
							} else {
								
								LOG.info("Incidente MCD#" + inc_mcd + " está mapeado para o incidente TLF#" + inc_tlf + ". Atualizando...");
								LOG.info("Executando getIncident: "+reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID") );							
								
								if (this.updateIncident(mcd_data, inc_hpdhd.get(0), inc_mcd)) {
									this.signIncidentAcceptance(inc_mcd, inc_tlf);
									LOG.info("Incidente TLF#" + inc_tlf + " atualizado com sucesso.");
									this.atualizaFrmIntermediario(reqnew.get("Request ID"), "VCIBUS0000I", inc_tlf, 1);
									//this.atualizaWorklogFormIntermediario(reqnew.get("Request ID"), inc_mcd);
									ret = true;
								} else {
									LOG.warn("Um erro ocorreu ao atualizar o incidente TLF#" + inc_tlf + " com os dados do incidente MCD#" + inc_mcd);						
								}
							}
							


						} else {
							//Cria um incidente
							LOG.info("[createOrUpdateIncident] Incidente MCD#" + inc_mcd + " não encontrado no Remedy, criando novo incidente...");		
							
							/*							
							JSONObject mcd_data = ITMass.getIncident(inc_mcd);							
							
							String[] inc_cop = this.validaCategoriaOperacional(mcd_data, inc_mcd);*/
														
							if (inc_cop == null) {
								this.atualizaFrmIntermediario(reqnew.get("Request ID"), MsgBarramentoEnum.CATEGORIA.getDescricaoErro(), inc_tlf, 2);
							} else {
								inc_tlf = this.createIncident(mcd_data, inc_mcd, inc_cop); 
								
								if (inc_tlf != null) {

									LOG.info("[createOrUpdateIncident] Incidente TLF#" + inc_tlf + " criado com sucesso.");
									this.createWorklogRemedy(inc_tlf, inc_mcd);
									this.atualizaFrmIntermediario(reqnew.get("Request ID"), "VCIBUS0000I", inc_tlf, 1);
									ret = true;
									LOG.info("------------------------------");
									LOG.info("MCD Incidente: " + inc_mcd);
									LOG.info("TLF Incidente ID: " + inc_tlf);
									LOG.info("------------------------------");

									cal = acessAPI.callBackIncidentNumberToRemedy(inc_mcd, inc_tlf);

									if(cal) { 
										LOG.info("[createOrUpdateIncident] Incidente foi gravado no MCD com sucesso.");
									}else{
										LOG.error("[createOrUpdateIncident] Houve um problema enviar o numero do incidente para MCD. " + "INCIDENTE: " + inc_mcd);
									}
								} else {
									LOG.warn("[createOrUpdateIncident] Um erro ocorreu ao criar um incidente TLF para o incidente MCD#" + inc_mcd);
									
								}									
							}						
						}						
						
					} catch (Exception e) {
						LOG.error("[createOrUpdateIncident] Um erro ocorreu ao tentar criar ou atualizar o incidente no Remedy. Mensagem: " + e.getMessage());
					}
					

				} else {
					LOG.warn("[createOrUpdateIncident] Busca pelo incidente MCD#" + inc_mcd + " no Remedy retornou nulo.");
				}
			
		} else {
			LOG.error("[createOrUpdateIncident] Falha no request REQ#" + reqnew.get("Request ID") + ". O campo VCI_INC_USRSRVREST_INCORIG_ID veio sem o número do incidente McDonalds informado.");
		}

		return ret;
	}

	/**
	 *	Cria os worklogs na HPD vindos da fujitsu
	 *	@param inc_tlf Número do incidente no Remedy
	 *	@param inc_mcd Número do chamado da fujitsu
	 */
	private void createWorklogRemedy(String inc_tlf, String inc_mcd){
		List<Worklog> listworklog = acessAPI.listWorklogs(inc_mcd);
		
		LOG.info("[createWorklog] - Criando o(s) Worklog(s)...");
		LOG.info("[createWorklog] - Será criado " + listworklog.size()+" Worklog(s) para o TLF#: " + inc_tlf);
		Collections.reverse(listworklog);

		try {
			for(Worklog worklog : listworklog ){
				remedyBO.createWorkInfo(worklog.getWork_description(), inc_tlf);
			}

			LOG.info("[createWorklog] - Criação de Worklog finalizou com sucesso.");
		} catch (Exception e) {
			LOG.error("[createWorklog] - Falha na criação de Worklog. Mensagem: " + e.getMessage());
		}
	}

	/**
	 *	Atualiza o form de integração com os workLogs da fujitsu
	 *	@param req_tlf Número do request ID
	 *	@param inc_tlf Número do incidente no Remedy
	 */
/*	private void atualizaWorklogFormIntermediario(String req_tlf, String inc_mcd){
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		List<Worklog> listworklog = acessAPI.listWorklogs(inc_mcd);
		Collections.reverse(listworklog);
		LOG.info("[atualizaWorklogFormIntermediario] - Atualizando o(s) Worklog(s)...");
		
		for(Worklog worklog : listworklog ){
			attrs.put("VCI_INC_USRSRVREST_WORKLOG", worklog.getWork_description());
		}
		
		try {
			remedyBO.updateEntry(Configuration.getConfig("mcd.100.2.1.formIncidentesIntegracao"), req_tlf, attrs);
			
			LOG.info("[atualizaWorklogFormIntermediario] - Atualização de Worklog finalizou com sucesso.");
		} catch (Exception e) {
			LOG.error("[atualizaWorklogFormIntermediario] - Falha na atualização de Worklog. Mensagem: " + e.getMessage());
		}
	}*/
	
	/**
	 * Atualiza o Form de integração
	 * @param p_req Número do request ID
	 * @param p_msg_cod Código da Mensagem
	 * @param p_incno Número do Incidente Remedy
	 * @param inc_retMcd Número do chamado fujitsu
	 */
	private void atualizaFrmIntermediario(String p_req, String p_msg_cod, String p_incno, int status) {
		String[] msgs_ret             = this.getMsgBarramento(p_msg_cod);
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		DateFormat df_bmc             = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.bmc.alt"));
		String date_value               = null;
		boolean atualizou_ok          = false;
				
		
		date_value = df_bmc.format(Calendar.getInstance().getTime());
		attrs.put("VCI_INC_USRSRVREST_DTRESPONSE", date_value);
		attrs.put("VCI_INC_USRSRVREST_CODERRO", msgs_ret[0]);
		attrs.put("VCI_INC_USRSRVREST_TXTERRO", msgs_ret[1]);
		attrs.put("Status", status);
		attrs.put("VCI_INC_USRSRVREST_INC_REMEDY", p_incno);

		try {
			atualizou_ok = remedyBO.updateEntry("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", p_req, attrs);
			
			if (atualizou_ok) {
				LOG.info("REQ#" + p_req + " do form intermediário atualizado com sucesso.");
			} else {
				LOG.warn("Um erro ocorreu ao atualizar status na REQ#" + p_req + ".");
			}

		} catch (Exception e) {
			LOG.error("Uma exception ocorreu ao atualizar status na REQ#" + p_req + ". Mensagem: " + e.getMessage());
		}
	}

	/**
	 *	Cria um incidente na HPD:HelpDesk
	 *	@param p_req Parâmetros do Incidente
	 *  @return Número do Incidente
	 */
	private String createIncident(JSONObject mcd_data, String inc_mcd, String[] inc_cop) {
		String ret                    = null;
		//JSONObject mcd_data           = null;
		//String inc_mcd                = null;
		String inc_notes              = null;
		String inc_cpy                = null;
		String inc_impact             = null;
		String inc_urgency            = null;
		String inc_status             = null;
		String inc_serv_type          = null;
		String inc_summary            = null;
		//String[] inc_cop              = null;

		try {
			
			//inc_mcd = p_req.get("VCI_INC_USRSRVREST_INCORIG_ID");
			LOG.info("[createIncident] Executando getIncident: " + inc_mcd );
			
			Incident incident_mcd = acessAPI.getIncident(inc_mcd);

			if (mcd_data.getString("Ticket Type").equalsIgnoreCase("Incident")) {
				
				
				inc_summary   = StringUtils.left(mcd_data.getString("Requester Name") + mcd_data.getString("Symptom Description"), 90);
				inc_notes     = mcd_data.getString("Symptom Details");
				inc_impact    = this.getImpact(mcd_data.getString("Impact"));
				inc_urgency   = this.getUrgency(mcd_data.getString("Urgency"));
				inc_status    = "0";
				inc_serv_type = "0";

				LOG.info("[createIncident] Criando incidente com parâmetros: summary=" + inc_summary + ", notes=" + inc_notes + ", company=" + inc_cpy +
						", tier1=" + inc_cop[0] + ", tier2=" + inc_cop[1] + ", tier3=" + inc_cop[2] + ", impact=" + inc_impact + ", urgency=" + inc_urgency + 
						", statusOUT=" + inc_status + ", serviceType=" + inc_serv_type + ", number=" + inc_mcd);
				
				
				
				
				ret = remedyBO.createIncident(inc_summary, inc_notes, inc_cpy, inc_cop[0], inc_cop[1], inc_cop[2], inc_impact, inc_urgency, inc_status, inc_serv_type, inc_mcd);
				acessAPI.acceptAssignment(incident_mcd, ret);
			} else {
				LOG.warn("[createIncident] Ticket Type <> 'Incident'. Ticket Type identificado: " + mcd_data.getString("Ticket Type"));
			}
		} catch (Exception e) {
			LOG.error("[createIncident] Um erro ocorreu ao tentar criar o incidente no Remedy. Mensagem: " + e.getMessage());
		}
		return ret;
	}

	private String[] validaCategoriaOperacional(JSONObject mcd_data, String inc_mcd) { 
		String[] returnCode = null;		
		
		LOG.info("[createIncident] Executando a validação da categoria operacional para o icidente: " + inc_mcd );
				
		if (mcd_data.getString("Ticket Type").equalsIgnoreCase("Incident")) {
			
			String[] inc_cop = this.getCategoriaOperacional(mcd_data.getString("Category"), mcd_data.getString("Type"));
			if (inc_cop != null && inc_cop.length > 0) {
				if (inc_cop[3] != null && !inc_cop[3].equalsIgnoreCase("N")) {					
					returnCode = inc_cop;					
				} else {
					LOG.warn("Categorizacao operacional não mapeada para o incidente " + inc_mcd);
				}
			} else {
				LOG.warn("Impossivel determinar a categorizacao operacional para o incidente " + inc_mcd);
			}
		}
		

		return returnCode;
	}

	private boolean updateIncident(JSONObject mcd_data, HashMap<String, String> p_inc_tlf, String inc_mcd) {
		boolean ret                   = false;
		//JSONObject mcd_data           = null;
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		String inc_notes              = null;
		String[] inc_res              = null;

		try {
			
			//mcd_data = ITMass.getIncident(p_req.get("VCI_INC_USRSRVREST_INCORIG_ID"));
			

			if (mcd_data.getString("Ticket Type").equalsIgnoreCase("Incident")) {

				// Só atualiza o Remedy se o incidente MCD for mais atual - mcd_data.getString("Last Modified"),
				if (IncMcdIsAfterIncTlf(mcd_data.getString("Last Modified"), p_inc_tlf.get("Last Modified Date"))) {
					// Notes
					//inc_notes = "Sintoma:\nDescrição: " + mcd_data.getString("Symptom Description") + "\nDetalhes: " + mcd_data.getString("Symptom Details") + "\n\nSolicitante:\nPrimeiro nome: " + mcd_data.getString("primeiro nome do solicitante") + "\nSobrenome: " + mcd_data.getString("sobrenome do solicitante") + "\n\nConectividade:\nAcessa Internet a partir do micro da gerência? " + mcd_data.getString("consegue acessar a internet a partir do micro da gerência?") + "\nAcessa Internet a partir do micro da sala de break? " + mcd_data.getString("consegue acessar a internet a partir do micro da sala de break?") + "\nRealiza transações TEF no pos? " + mcd_data.getString("consegue realizar transações tef no pos?");
					//inc_notes = ""; 
					
					inc_notes = ITMass.getAllCustomAttributesInOneField(inc_mcd);
					
					attrs.put("Detailed Decription", inc_notes);					

					// Impact
					attrs.put("Impact", this.getImpact(mcd_data.getString("Impact")));

					// Urgency
					attrs.put("Urgency", this.getUrgency(mcd_data.getString("Urgency")));

					// Resolution
					attrs.put("Resolution", mcd_data.getString("Resolution"));

					if (mcd_data.getString("Status").equalsIgnoreCase("Resolved")) {
						inc_res = this.getResolution("Default");
						attrs.put("Resolution Category", inc_res[0]);
						attrs.put("Resolution Category Tier 2", inc_res[1]);
						attrs.put("Resolution Category Tier 3", inc_res[2]);
					}

					// Atualiza o incidente
					remedyBO.updateEntry("HPD:Help Desk", p_inc_tlf.get("Entry ID"), attrs);
					
					
				} else {
					LOG.info("Registro TLF é mais atual que o incidente McD.");
				}

				ret = true;
			} else {
				LOG.warn("Registro se refere a [" + mcd_data.getString("Ticket Type") + "], não a incidente.");
			}

		} catch (Exception e1) {
			LOG.error("Impossível coletar incidente na plataforma ITMass. Mensagem: " + e1.getMessage());
		}
		
		return ret;
	}

	private String[] getResolution(String p_resolucao_mcd) {				
		//recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS", "'536870925' = 4 AND '7' = 0 AND '536870923' = \"" + p_resolucao_mcd + "\"");

		String[] ret = new String[3];
		ret = categsrv.getMapMcD(String.valueOf(McDMappingTipoMap.Cat_Res.ordinal()), p_resolucao_mcd, "", "", "", false);
				
		return ret;
	}

	/**
	 *	Faz o DE/PARA da categoria operacional no form "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS" 
	 *	@param p_category_mcd Categoria do incidente da Fujitsu
	 *	@param p_type_mcd Tipo de incidente da Fujitsu
	 *  @return categorização
	 */
	private String[] getCategoriaOperacional(String p_category_mcd, String p_type_mcd) {		
		//recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS", "'536870925' = 0 AND '7' = 0 AND '536870923' = \"" + p_category_mcd + "\" AND '536870926' = \"" + p_type_mcd + "\"");
		//recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS", "'Tipo de Incidente' = \"Default\"");

		String[] ret = categsrv.getMapMcD(String.valueOf(McDMappingTipoMap.Cat.ordinal()), p_category_mcd, p_type_mcd, "", "", true);
		
		if (ret != null && ret.length > 0) {
			return ret;
		}
		
		return null;
	}
	
	/**
	 *	Faz o DE/PARA do impacto do incidente no form "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS" 
	 *	@param p_impact_mcd Impacto do incidente da Fujitsu
	 *  @return impacto 
	 */
	private String getImpact(String p_impact_mcd) {		
		//recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS", "'536870925' = 2 AND '7' = 0 AND '536870923' = \"" + p_impact_mcd + "\"");
		
		String[] ret = new String[3];
		ret = categsrv.getMapMcD(String.valueOf(McDMappingTipoMap.Impacto.ordinal()), p_impact_mcd, "", "", "", false);
		
		return ret[0];
	}

	/**
	 *	Faz o DE/PARA da Urgência do incidente no form "IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS" 
	 *	@param p_urgency_mcd Urgência do incidente da Fujitsu
	 *  @return Urgência
	 */
	
	private String getUrgency(String p_urgency_mcd) {		
		//recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MAPEAMENTO_ATRIBS", "'536870925' = 3 AND '7' = 0 AND '536870923' = \"" + p_urgency_mcd + "\"");

		String[] ret = new String[3];
		ret = categsrv.getMapMcD(String.valueOf(McDMappingTipoMap.Urgencia.ordinal()), p_urgency_mcd, "", "", "", false);
		
		return ret[0];
	}

	/**
	 *	Busca incidentes com Status "Novo" no form de integração
	 * @return Lista de incidentes
	 */
	private List<HashMap<String, String>> getNewIncidents() {
		List<HashMap<String, String>> listaIncidentesNovos = null;
		
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(536870948, "VCI_INC_USRSRVREST_INCORIG_ID");
		fields.put(1, "Request ID");
		
		listaIncidentesNovos = remedyBO.frmQuery("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", "'7' = \"Novo\"", fields); 
		return listaIncidentesNovos;
	}

	/**
	 * Verifica se a data do chamado da fujitsu é posterior a do Remedy
	 * @param p_date_mcd Data chamado fujitsu
	 * @param p_date_bmc Data Incidente Remedy
	 * @return Validação do tipo boolean
	 */
	private boolean IncMcdIsAfterIncTlf(String p_date_mcd, String p_date_bmc) throws Exception {
		boolean ret       = false;
		Date mcd          = null;
		Date bmc          = null;
		DateFormat df_mcd = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.mcd"));
        DateFormat df_bmc = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.out"));

        try {
			mcd = df_mcd.parse(p_date_mcd);
		} catch (ParseException e) {
			e.printStackTrace();
		}

        try {
        	bmc = df_bmc.parse(p_date_bmc);
		} catch (ParseException e) {
			e.printStackTrace();
		}

        if (mcd != null) {
        	if (bmc != null) {
        		ret = mcd.after(bmc);
        		LOG.info("[IncMcdIsAfterIncTlf] Data MCD[" + mcd.toString() + "] é posterior a data TLF[" + bmc.toString() + "]? [" + ret + "]");
        	} else {
        		throw new Exception ("[IncMcdIsAfterIncTlf] Impossível verificar data de última modificação do incidente TLF. Valor [" + p_date_bmc + "] é uma data inválida.");
        	}
        } else {
        	throw new Exception ("[IncMcdIsAfterIncTlf] Impossível verificar data de última modificação do incidente McD. Valor [" + p_date_mcd + "] é uma data inválida.");
        }
		return ret;
	}

	/**
	 * Faz o DE/PARA para a mensagem do barramento no form "IA:MCDONALDS:V01:MSG_BARRAMENTO"
	 * @param p_msg_cod Código da mensagem 
	 * @return Código e descrição do erro
	 */
	private String[] getMsgBarramento(String p_msg_cod) {
		String[] ret = new String[2];
		List<HashMap<String, String>> recs = null;
		
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(8, "COD_ERRO");
		fields.put(536870916, "DS_ERRO");	
				
		recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MSG_BARRAMENTO", "'7' = 0 AND '8' = \"" + p_msg_cod + "\"", fields);

		if (recs != null && recs.size() > 0) {
			ret[0] = recs.get(0).get("COD_ERRO");
			ret[1] = recs.get(0).get("DS_ERRO");
		} else {
			ret[0] = "COD_NAO_ENC";
			ret[1] = "TXT_NAO_ENCONTRADO";
		}

		return ret;
	}

	private void signIncidentAcceptance(String inc_mcd, String inc_tlf) {
		// TODO Auto-generated method stub
		
	}
}
