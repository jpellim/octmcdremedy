package br.com.ia.oct.mcd.remedy.register;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import br.com.ia.oct.mcd.mgmt.register.MgmtRegister;
import br.com.ia.oct.mcd.remedy.CategorizationServiceImpl;
import br.com.ia.oct.mcd.remedy.McdToRemedyProcessorBean;
import br.com.ia.oct.mcd.remedy.RemedyBOServiceImpl;
import br.com.ia.oct.mcd.remedy.RemedyDAOServiceImpl;
import br.com.ia.oct.mcd.remedy.RemedyGenerateCSVBean;
import br.com.ia.oct.mcd.remedy.RequestStatusServiceImpl;

@Stateful
@Remote(RemedyRegister.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/register")
@EJB(name = "octopus/mcd/remedy/register", beanInterface = RemedyRegister.class) 
public class RemedyRegisterBean implements RemedyRegister {
	//@EJB //(mappedName="octopus/mcd/management/register")
	@Inject
	private MgmtRegister register;
	
	public void doRegister() throws Exception {
		register.registerComponent(RemedyDAOServiceImpl.class);
		register.registerComponent(RemedyBOServiceImpl.class);
		register.registerComponent(CategorizationServiceImpl.class);
		register.registerComponent(RequestStatusServiceImpl.class);
		register.registerComponent(McdToRemedyProcessorBean.class);
		register.registerComponent(RemedyGenerateCSVBean.class);
	}

}

