package br.com.ia.oct.mcd.remedy;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.inteqnet.core.appservices.webservice.beans.xsd.Worklog;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.MsgBarramentoEnum;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.itmass.ITMassAPI;
import br.com.ia.oct.mcd.itmass.IncidentOperations;

@Stateless
@Remote(RemedyToMcD.class)
@Local(RemedyToMcDLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/remedy/remedytomcd")
@EJB(name = "octopus/mcd/remedy/remedytomcd", beanInterface = RemedyToMcD.class) 
@OCTJob(uid="4.5.2", nome="RemedyToMcDBean", descricao="Job para processamento de registros (TLF -> McD) do form intermediario", jndiLookup="octopus/mcd/remedy/remedytomcd", clazz="", cronTrigger="0 0/1 * 1/1 * ? *", grupo="REMEDY", ativo=false, node="node03")
@OCTComponent(version="$LastChangedRevision$",uid="4.5.2'", nome="RemedyToMcDBean", descricao="JOB para processamento de registros (TLF -> McD) do form intermediario", jndiLookup="MCD/RemedyToMcDBean/local", clazz="", cronTrigger="0 0/1 * 1/1 * ? *", grupo="REMEDY", ativo=false)
@TransactionTimeout(value=3300) // 55 minutos
public class RemedyToMcDBean implements RemedyToMcD, RemedyToMcDLocal {
	private static final long serialVersionUID = 1L;
	private static boolean running  = false;
	private static final Logger LOG = Logger.getLogger(RemedyToMcDBean.class);

	//@EJB  //(mappedName="octopus/mcd/remedy/boservice")
	@Inject
	private RemedyBOService remedyBO;

//	@EJB  //(mappedName="octopus/mcd/itmass/incidentops")
	@Inject
	private IncidentOperations ITMass;

	public Boolean isAlive() {
		return true;
	}

	@PostConstruct
	public void inicializa () {
		OCTUtils.setProxyConfig();		
	}

	public boolean execute(HashMap<String, Object> params) throws Exception {
		if (running) {
			LOG.info("Job anterior ainda em execuÃ§Ã£o, saindo...");
			return true;
		}

		running     = true;
		boolean ret = false;
		LOG.info("[RemedyToMcD] Job - Inicio");
		
		List<HashMap<String, String>> incList = null;
		
		try {
			incList = this.getIncidentsForReturn();
		} catch (Exception e){
			LOG.error("[RemedyToMcD] Ocorreu um problema de comunicaÃ§Ã£o com o Remedy...");
			running = false;
			return true;
		}
		//Verifica se possui incidents a serem atualizado
		if (incList != null) {
			if (incList.size() > 0) {
				for (HashMap<String, String> incFrmInt : incList) {
					LOG.info("[RemedyToMcD] Processando TLF#" + incFrmInt.get("VCI_INC_USRSRVREST_INC_ID"));
					if (createOrUpdateIncidentOnMcD(incFrmInt)) {
						LOG.info("[RemedyToMcD] TLF#" + incFrmInt.get("VCI_INC_USRSRVREST_INC_ID") + " processado com sucesso.");
					} else {
						LOG.warn("[RemedyToMcD] Erro ao processar TLF#" + incFrmInt.get("VCI_INC_USRSRVREST_INC_ID"));
					}
				}
			} else {
				LOG.info("[RemedyToMcD] A busca por requests com status MCDONALDS - ATUALIZAR retornou 0 (zero) registros.");
			}
		} else {
			LOG.info("[RemedyToMcD] A busca por requests com status MCDONALDS - ATUALIZAR retornou nulo.");
		}
		LOG.info("[RemedyToMcD] Job - Fim");
		running = false;
		return ret;
	}
	
	
	private void ajustarIncMcdFrmIntermediario(String inc_tlf, String inc_mcd, String reqID) {
				
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("VCI_INC_USRSRVREST_INCORIG_ID", inc_mcd);		
		
		try {
			Boolean atualizou_ok = remedyBO.updateEntry("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", reqID, attrs);
			if (!atualizou_ok) {
				LOG.info("NÃ£o foi possÃ­vel ajustar a REQ#" + reqID + " com o valor do incidente McD#" + inc_mcd);
			}
		} catch(Exception e) {
			LOG.info("Erro ao ajustar a REQ#" + reqID + " com o valor do incidente McD#" + inc_mcd);			
		}
		
		LOG.info("Ajuste na REQ#" + reqID + " atualizado com o nÃºmero do incidente McD#" + inc_mcd);
	}
	
	
	/**
	 * Cria ou atualiza um incidente no Remedy 
	 * @param Um incidente  
	 * @return A verificaÃ§Ã£o do processamento do tipo boolean
	 */
	private boolean createOrUpdateIncidentOnMcD(HashMap<String, String> p_reqInt) {
		boolean ret              = false;
		String inc_mcd           = p_reqInt.get("VCI_INC_USRSRVREST_INCORIG_ID");
		String inc_tlf           = p_reqInt.get("VCI_INC_USRSRVREST_INC_REMEDY");
		String inc_retMcd        = null;
		String p_randomTxt       = null;
		IncidentActions situacao = null;

		try {
			p_randomTxt = OCTUtils.getRandomString(100, false);
		} catch (Exception e1) {
			 LOG.error("[RemedyToMcD] Falha ao gerar String Random");
		}
		
		
		if ((inc_mcd == null) || (inc_mcd.trim().length() == 0)){	
			
			//validando se o incidente nÃ£o possui esse campo preenchido
			HashMap<Integer, String> fields = new HashMap<Integer, String>();
			fields.put(1000000652, "Vendor Ticket Number");
			
			List<HashMap<String, String>> inc_hpdhd = remedyBO.frmQuery("HPD:Help Desk", "'Incident Number' = \"" + inc_tlf + "\"", fields);			
			
			if ((inc_hpdhd != null) && (inc_hpdhd.size() > 0)) {
				String inc_mcd_aux = inc_hpdhd.get(0).get("Vendor Ticket Number");
				if ((inc_mcd_aux != null) && (inc_mcd_aux.length() > 0)){
					inc_mcd = inc_mcd_aux;					
					p_reqInt.put("VCI_INC_USRSRVREST_INCORIG_ID", inc_mcd);
					
					LOG.info("Incidente tlf: " + inc_tlf + ", encontrou incidente jÃ¡ criado para o McD: " + inc_mcd + ", atualizando formulÃ¡rio intermediÃ¡rio com novo valor");
					ajustarIncMcdFrmIntermediario(inc_tlf, inc_mcd, p_reqInt.get("Request ID"));					
				}
			}			
		}
		

		ITMassAPI itMassAPI = new ITMassAPI();
		itMassAPI.init();

		if (inc_mcd != null && inc_mcd.trim().length() > 0 ) {
			situacao = this.getOperation(p_reqInt);
			LOG.info("[createOrUpdateIncidentOnMcD] Executando a chamada: " + situacao);
			switch (situacao) {
				case ALERTAR_REQUEST_INVALIDO:
					LOG.error("[RemedyToMcD] Um request invalido foi enviado para avaliaÃ§Ã£o. A criaÃ§Ã£o/atualizaÃ§Ã£o nÃ£o serÃ¡ enviada. Entre em contato com os administradores da aplicaÃ§Ã£o.");
					break;
				case ALERTAR_SIT_DESCONHECIDA:
					LOG.error("[RemedyToMcD] Uma operaÃ§Ã£o desconhecida foi detectada ao enviar o request para a Fujitsu. A criaÃ§Ã£o/atualizaÃ§Ã£o nÃ£o serÃ¡ enviada (VCI_INC_USRSRVREST_HASH = " + p_reqInt.get("VCI_INC_USRSRVREST_HASH") + ")");
					break;
				case ALERTAR_STATUS_INCIDENTE_NULO:
					LOG.error("[RemedyToMcD] O status do incidente Remedy nÃ£o foi detectado. A criaÃ§Ã£o/atualizaÃ§Ã£o nÃ£o serÃ¡ enviada (VCI_INC_USRSRVREST_HASH = " + p_reqInt.get("VCI_INC_USRSRVREST_HASH") + ")");
					break;
				case CRIAR_WORKLOG:
					LOG.info("Criando Worklog incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf);
					try {
						if (createWorklog(p_reqInt)){ 
							LOG.info("[createOrUpdateIncident] Worklog criado");
							this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
							
						}
						else {
							
							LOG.warn("Um erro ocorreu ao criar worklog no incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
							this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
						}
					} catch (Exception e) {
						LOG.warn("Uma exceÃ§Ã£o ocorreu ao criar worklog no incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
						LOG.error(e.getMessage());
						this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
					} 
						
					break;
				case ENVIAR_ATUALIZACAO:
					LOG.info("Atualizando incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf);
					try {
						if (this.updateIncidentMcD(p_reqInt, inc_mcd)) { 
							LOG.info("Incidente McD#" + inc_mcd + " atualizado com sucesso!");
							this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
							ret = true;
						} else {
							LOG.warn("Um erro ocorreu ao atualizar o incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
							this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);			
						}
					} catch (RemoteException e) {
						LOG.warn("Uma exceÃ§Ã£o ocorreu ao atualizar o incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
						LOG.error(e.getMessage());
					}
					break;
				case ENVIAR_PENDING:
					LOG.info("Passando incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf + ", para o status 'Pending'...");
					try {
						if (itMassAPI.ticketForPending(p_reqInt)) { 
							LOG.info("Incidente McD#" + inc_mcd + " atualizado com sucesso!");
							this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
							ret = true;
						} else {
							LOG.warn("Um erro ocorreu ao atualizar o incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
							this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
						}
					} catch (Exception e) {
						LOG.error("ExceÃ§Ã£o ao tentar resolver o incidente no McD. JVM: " + e.getMessage());
						this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
					}
					break;
				case ENVIAR_RESOLVED:
					LOG.info("Resolvendo incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf);

					try {
						if (itMassAPI.resolveTicket(inc_mcd, p_reqInt.get("INC-TFL_Descricao_Resolucao"))) {
							LOG.info("Incidente McD#" + inc_mcd + " atualizado com sucesso!");
							this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
							ret = true;
						} else {
							LOG.warn("Um erro ocorreu ao atualizar o incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
							this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
						}
					} catch (Exception e) {
						LOG.error("ExceÃ§Ã£o ao tentar resolver o incidente no McD. JVM: " + e.getMessage());
						this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
					}
					break;
				case ENVIAR_CANCELED:
					LOG.info("Passando incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf + ", para o status 'Canceled'...");
					try {
						if (itMassAPI.ticketForCanceled(p_reqInt)) { //////// ALTERAR 
							LOG.info("Incidente McD#" + inc_mcd + " atualizado com sucesso!");
							this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
							ret = true;
						} else {
							LOG.warn("Um erro ocorreu ao atualizar o incidente McD#" + inc_mcd + " com os dados do incidente TLF#" + inc_tlf);
							this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
						}
					} catch (Exception e) {
						LOG.error("ExceÃ§Ã£o ao tentar cancelar o incidente no McD. JVM: " + e.getMessage());
						this.atualizaFrmIntermediarioIncErro(p_reqInt.get("Request ID"),"VCIBUS0001E", inc_tlf, inc_retMcd, 5, p_randomTxt);
					}
					break;
				default:
					LOG.warn("AÃ§Ã£o desconhecida no incidente McD#" + inc_mcd + ", mapeado para incidente TLF#" + inc_tlf + ".");
					break;
			}

		} else {
			// Cria o incidente
			//Verifica se nÃ£o possui worklog
			if( ! "WLCreate".equalsIgnoreCase(p_reqInt.get("VCI_INC_USRSRVREST_NOTIF_STATUS1"))) {
				inc_retMcd = this.createIncidentMcD(p_reqInt);
			}
			if (inc_retMcd != null && (inc_retMcd.trim().length() > 0 )) {
				LOG.info("Incidente criado no McDonalds com sucesso!");
				this.atualizaFrmIntermediario(p_reqInt.get("Request ID"), 4, "VCIBUS0000I", inc_tlf, inc_retMcd);
				ret = true;
			} else {
				LOG.warn("Um erro ocorreu ao criar o incidente McDonalds com os dados do incidente TLF#" + inc_tlf);
			} 
		}
		return ret;
	}
	
	/**
	 * Verifica o tipo de operaÃ§Ã£o do Incidente
	 * @param p_req ParÃ¢metros do Incidente
	 * @return Enum com o tipo de operaÃ§Ã£o
	 */
	private IncidentActions getOperation(HashMap<String, String> p_req) {
		IncidentActions ret = null;
		String status_req   = null;

		if (p_req == null || p_req.size() == 0) {
			ret = IncidentActions.ALERTAR_REQUEST_INVALIDO;
		} else {
			if ("WLCreate".equalsIgnoreCase(p_req.get("VCI_INC_USRSRVREST_NOTIF_STATUS1"))) {
				ret = IncidentActions.CRIAR_WORKLOG;
			} else {
				status_req = p_req.get("VCI_INC_USRSRVREST_HASH").trim().toLowerCase();

				if (status_req == null || status_req.length() <= 0) {
					ret = IncidentActions.ALERTAR_STATUS_INCIDENTE_NULO;
				} else {
					switch (status_req) {
						case "resolved":
							ret = IncidentActions.ENVIAR_RESOLVED;
							break;
						case "pending":
							ret = IncidentActions.ENVIAR_PENDING;
							break;
						case "cancelled":
							ret = IncidentActions.ENVIAR_CANCELED;
							break;
						default:
							ret = IncidentActions.ENVIAR_ATUALIZACAO;
							break;
					}
				}
			}

			if (ret == null) {
				ret = IncidentActions.ALERTAR_SIT_DESCONHECIDA;
			}
		}
		return ret;
	}
	
	/**
	 * Cria um incidente na Fujitsu 
	 * @param p_req ParÃ¢metros do Incidente 
	 * @return NÃºmero do chamado criado na Fujitsu
	 */
	private String createIncidentMcD(HashMap<String, String> p_req) {
		String inc   = null;
		String nlj   = null;
		
		HashMap<String, String> inc_hpd = this.getIncidentRemedy(p_req.get("VCI_INC_USRSRVREST_INC_ID"));

		if (p_req != null && p_req.size() > 0) {
			if (inc_hpd != null && inc_hpd.size() > 0) {
	
				try {
					
					LOG.info("[createIncidentMcD] Valida categoria operacional para integraÃ§Ã£o.");
					String[] categorization = ITMass.validaCategoriaOperacional(inc_hpd);
					
					if (categorization != null) {

						//Ambiente fujitsu ProduÃ§Ã£o
						if("prod".equals(Configuration.getConfig("mcd.fujitsu.ambiente"))){
							//Para pegar o cÃ³digo da loja fornecido pelo Solarwinds
							String codigo = this.buscaCodigoLoja(inc_hpd.get("Detailed Decription"));
							String nome = this.buscaNomeLoja(codigo);
						
							nlj = codigo+","+nome;
							
						}
						//Ambiente fujitsu HomologaÃ§Ã£o
						else {
							nlj = Configuration.getConfig("mcd.100.1.1.RequesterName");
						}
						
						if (nlj != null) {
							LOG.info("[createIncidentMcD] Loja encontrada: " + nlj);
						} 					
						
						LOG.info(" [createIncidentMcD] Criando incidente no McD com os parametros: " +"\n"+"FORM INTERMEDIÃ�RIO:" +"\n"+ 
					               p_req.get("VCI_INC_USRSRVREST_INC_ID")+"\n"+
					               p_req.get("Request ID")+"\n"+
					               p_req.get("Status")+"\n"+
					               p_req.get("NUM_REQUESTS")+"\n"+
					               "HPD:HELP DESK"+"\n"+
					               inc_hpd.get("Detailed Decription")+"\n"+
					               inc_hpd.get("Contact Company")+"\n"+
					               inc_hpd.get("Categorization Tier 1")+"\n"+
					               inc_hpd.get("Categorization Tier 2")+"\n"+
					               inc_hpd.get("Categorization Tier 3")+"\n"+
					               "LOJA: "+ nlj +"\n");						
						
						
						inc = ITMass.createIncident(p_req, inc_hpd, nlj,categorization);
						LOG.info("[createIncidentMcD] Retorno do createIncident: " + inc);
						
						if (inc != null && inc.trim().length() > 0) {
							this.atualizaFrmHelpDesk(p_req.get("VCI_INC_USRSRVREST_INC_REMEDY"), inc, inc_hpd.get("Entry ID"));
						} else {
							LOG.warn("[createIncidentMcD] Incidente parece ter sido criado, mas nÃ£o retornou um nÃºmero vÃ¡lido.");
						}
					} else {
						LOG.warn("Categorizacao operacional para o incidente TLF#" + inc_hpd.get("Incident Number")+" nÃ£o deve ser integrada de acordo com configuraÃ§Ã£o.");
						this.atualizaFrmIntermediarioIncErro(p_req.get("Request ID"), MsgBarramentoEnum.CATEGORIA.getDescricaoErro(), 
								p_req.get("VCI_INC_USRSRVREST_INC_REMEDY"), inc, 5, null);
					}
					
					

				} catch (Exception e) {
					LOG.error("[createIncidentMcD] Uma exception ocorreu durante a criaÃ§Ã£o do incidente na plataforma McD. Mensagem: " + e.getMessage());
					e.printStackTrace();
				}				
			} else {
				LOG.warn("[createIncidentMcD] Incidente nulo ou vazio para realizar a criaÃ§Ã£o na plataforma McDonalds.");
			}
		} else {
			LOG.warn("[createIncidentMcD] RequisiÃ§Ã£o nula ou vazia para realizar a criaÃ§Ã£o na plataforma McDonalds.");
		}
		return inc;
	}
	
	/**
	 * Busca o cÃ³digo da loja do MCD dentro do campo "Detailed Decription"
	 * do form HPD:HelpDesk
	 * @param swDesc ConteÃºdo do campo "Detailed Decription"
	 * @return CÃ³digo da loja
	 */
	private String buscaCodigoLoja(String swDesc){
		String procura = "Nome do site: ";
		String codigo = "";
		
		try	{
			int pos = swDesc.indexOf(procura) + procura.length() ;
			codigo = swDesc.substring(pos,(pos+3));
			LOG.info("Codigo da loja: " + codigo);
		
			} catch (Exception e){
				LOG.error("Erro ao fazer substring: "+ e.getMessage());
			}
		return codigo;
	}
	
	/**
	 * Busca no form "SIT:Site" do Remedy o nome da loja baseado no cÃ³digo
	 * @param codigo CÃ³digo da loja
	 * @return Nome da loja
	 */
	private String buscaNomeLoja(String codigo) {
		String ret = "";
		HashMap<String, String> con = null;
		String p_frm = "SIT:Site";
		String query = "'Location ID' = \"" + codigo + "\" AND 'Status' = \"Enabled\"" ;
				
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(1000000000, "Description");
		
		List<HashMap<String, String>> sites = remedyBO.frmQuery(p_frm, query, fields);
		
		if	(sites != null && sites.size() > 0){
			LOG.info("Busca em " + p_frm + " retornou " + sites.size() + " registros.");
			con = sites.get(0);
			ret = con.get("Description");
		} else {
			LOG.warn("Busca em " + p_frm + " retornou nulo ou vazio.");
		}
		return ret;
	}
	
	/**
	 * Atualiza um Incidente na Fujitsu
	 * @param p_req ParÃ¢metros do Incidente 
	 * @return VerificaÃ§Ã£o da atualizaÃ§Ã£o do tipo boolean
	 */
	private boolean updateIncidentMcD(HashMap<String, String> p_req, String inc_mcd) throws RemoteException {
		boolean ret = false;
		HashMap<String, String> inc_hpd = this.getIncidentRemedy(p_req.get("VCI_INC_USRSRVREST_INC_ID"));
		
		if (p_req != null && p_req.size() > 0) {
			if (inc_hpd != null && inc_hpd.size() > 0) {
				try {
					
					LOG.info("[createIncidentMcD] Valida categoria operacional para integraÃ§Ã£o.");
					String[] categorization = ITMass.validaCategoriaOperacional(inc_hpd);
					
					if (categorization != null) {
						ret = ITMass.updateIncident(p_req, inc_hpd);
						
					} else {
						LOG.warn("Categorizacao operacional para o incidente TLF#" + inc_hpd.get("Incident Number")+" nÃ£o deve ser integrada de acordo com configuraÃ§Ã£o.");
						this.atualizaFrmIntermediarioIncErro(p_req.get("Request ID"), MsgBarramentoEnum.CATEGORIA.getDescricaoErro(), 
								p_req.get("VCI_INC_USRSRVREST_INC_REMEDY"), inc_mcd, 5, null);
					}
				} catch (Exception e) {
					LOG.error("[updateIncidentMcD] Uma exception ocorreu durante a atualizaÃ§Ã£o do incidente na plataforma McD. Mensagem: " + e.getMessage());
					e.printStackTrace();
				}				
			} else {
				LOG.warn("[updateIncidentMcD] Incidente nulo ou vazio para realizar a atualizaÃ§Ã£o na plataforma McDonalds.");
			}
		} else {
			LOG.warn("[updateIncidentMcD] RequisiÃ§Ã£o nula ou vazia para realizar a atualizaÃ§Ã£o na plataforma McDonalds.");
		}
		return ret;
	}
	
	/**
	 * Busca parÃ¢metros de um incidente no Form HPD:HelpDesk
	 * @param p_incno NÃºmero do Incidente 
	 * @return ParÃ¢metros do Incidente na HPD:HelpDesk
	 */
	private HashMap<String, String> getIncidentRemedy(String p_incno) {
		HashMap<String, String> ret = null;
		
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(1000000151, "Detailed Description");
		fields.put(1000000082, "Contact Company");
		fields.put(1000000063, "Categorization Tier 1");
		fields.put(1000000064, "Categorization Tier 2");
		fields.put(1000000065, "Categorization Tier 3");
		fields.put(1, "Entry ID");
		fields.put(1000000000, "Description");
		fields.put(1000000161, "Incident Number");
		fields.put(1000002488, "Resolution Category");
		fields.put(1000003889, "Resolution Category Tier 2");
		fields.put(1000003890, "Resolution Category Tier 3");
						
		List<HashMap<String, String>> recs = remedyBO.frmQuery("HPD:Help Desk", "'Incident Number' = \"" + p_incno + "\"", fields); 

		if (recs != null && recs.size() > 0) {
			LOG.info("[RemedyToMcD] Busca incidente TLF#" + p_incno + " retornou " + recs.size() + " incidente(s).");
			ret = recs.get(0);
		} else {
			LOG.warn("[RemedyToMcD] Busca incidente TLF#" + p_incno + " retornou nulo ou vazio.");
		}
		return ret;
	}
	
	/**
	 * Atualiza o Form de integraÃ§Ã£o com Erro
	 * @param p_req NÃºmero do request ID
	 * @param p_msg_cod CÃ³digo da Mensagem
	 * @param p_incno NÃºmero do Incidente Remedy
	 * @param inc_retMcd NÃºmero do chamado fujitsu
	 * @param p_randomTxt Texto randÃ´mico
	 */
	private void atualizaFrmIntermediarioIncErro(String p_req, String p_msg_cod, String p_incno, String inc_retMcd, int status, String p_randomTxt) {
		String[] msgs_ret             = this.getMsgBarramento(p_msg_cod);
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		DateFormat df_bmc             = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.bmc.alt"));
		String date_value               = null;
		boolean atualizou_ok          = false;

		date_value = df_bmc.format(Calendar.getInstance().getTime());
		attrs.put("VCI_INC_USRSRVREST_DTRESPONSE", date_value);
		attrs.put("VCI_INC_USRSRVREST_CODERRO", msgs_ret[0]);
		attrs.put("VCI_INC_USRSRVREST_TXTERRO", msgs_ret[1]);
		attrs.put("VCI_INC_USRSRVREST_INC_REMEDY", p_incno);
		attrs.put("VCI_INC_USRSRVREST_INCORIG_ID", inc_retMcd);
		attrs.put("Status", status);
		attrs.put("STR_COUNTER",p_randomTxt);

		try {
			atualizou_ok = remedyBO.updateEntry("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", p_req, attrs);
			
			if (atualizou_ok) {
				LOG.info("REQ#" + p_req + " do form intermediÃ¡rio atualizado com sucesso.");
			} else {
				LOG.warn("Um erro ocorreu ao atualizar status na REQ#" + p_req + ".");
			}

		} catch (Exception e) {
			LOG.error("Uma exception ocorreu ao atualizar status na REQ#" + p_req + ". Mensagem: " + e.getMessage());
		}
	}
	
	/**
	 * Atualiza o Form de integraÃ§Ã£o
	 * @param p_req NÃºmero do request ID
	 * @param p_idx_status CÃ³digo do status
	 * @param p_msg_cod CÃ³digo da Mensagem
	 * @param p_incno NÃºmero do Incidente Remedy
	 * @param inc_retMcd NÃºmero do chamado fujitsu
	 */
	private void atualizaFrmIntermediario(String p_req, int p_idx_status, String p_msg_cod, String p_incno, String inc_retMcd) {
		String[] msgs_ret             = this.getMsgBarramento(p_msg_cod);
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		DateFormat df_bmc             = new SimpleDateFormat(Configuration.getConfig("mcd.date.format.bmc.alt"));
		String date_value             = null;
		boolean atualizou_ok          = false;

		date_value = df_bmc.format(Calendar.getInstance().getTime());
		attrs.put("VCI_INC_USRSRVREST_DTRESPONSE", date_value);
		attrs.put("VCI_INC_USRSRVREST_CODERRO", msgs_ret[0]);
		attrs.put("VCI_INC_USRSRVREST_TXTERRO", msgs_ret[1]);
		attrs.put("Status", p_idx_status);
		attrs.put("VCI_INC_USRSRVREST_INC_REMEDY", p_incno);
		attrs.put("VCI_INC_USRSRVREST_INCORIG_ID", inc_retMcd);

		try {
			atualizou_ok = remedyBO.updateEntry("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", p_req, attrs);
			
			if (atualizou_ok) {
				LOG.info("[atualizaFrmIntermediario] REQ#" + p_req + " do form intermediÃ¡rio atualizado com sucesso.");
			} else {
				LOG.warn("[atualizaFrmIntermediario] Um erro ocorreu ao atualizar status na REQ#" + p_req + ".");
			}

		} catch (Exception e) {
			LOG.error("[atualizaFrmIntermediario] Uma exception ocorreu ao atualizar status na REQ#" + p_req + ". Mensagem: " + e.getMessage());
		}
	}

	/**
	 * Atualiza o Form HPD:HelpDesk com o nÃºmero do chamado da fujitsu
	 * @param p_req NÃºmero do Incidente Remedy 
	 * @param p_vendor_tn NÃºmero do chamado criado na fujitsu
	 * @return VerificaÃ§Ã£o da atualizaÃ§Ã£o no tipo boolean
	 */
	private boolean atualizaFrmHelpDesk(String p_req, String p_vendor_tn, String entryID) {
		HashMap<String, Object> attrs = new HashMap<String, Object>();
		boolean ret                   = false;
		attrs.put("Vendor Ticket Number", p_vendor_tn);

		try {
			//String entryID = getEntryIDHelpDesk(p_req);
			LOG.info("[atualizaFrmHelpDesk] Atualizando HDP:HelpDesk #Incidente: " + p_req + " Entry ID: " +entryID);
			ret = remedyBO.updateEntry("HPD:Help Desk", entryID, attrs);
			if (ret) {
				LOG.info("[atualizaFrmHelpDesk] TLF#" + p_req + " agora possui atributo Vendor Ticket Number.");
			} else {
				LOG.warn("[atualizaFrmHelpDesk] Um erro ocorreu ao atualizar o atributo Vendor Ticket Number no incidente TLF#" + p_req + ".");
			}
		} catch (Exception e) {
			LOG.error("[atualizaFrmHelpDesk] Uma exception ocorreu ao atualizar o atributo Vendor Ticket Number no incidente TLF#" + p_req + ". Mensagem: " + e.getMessage());
		}
		return ret;
	}
	
	/**
	 * Busca o Entry ID na HPD:HelpDesk
	 * @param p_req NÃºmero do Incidente Remedy 
	 * @return NÃºmero da EntryID
	 */
//	private String getEntryIDHelpDesk(String p_req) {
//		String ret = "";
//		List<HashMap<String, String>> recs = null;
//		recs = remedyBO.frmQuery("HPD:Help Desk","'1000000161' = \"" + p_req + "\"");
//		ret= recs.get(0).get("Entry ID");
//		return ret;
//	}
	
	/**
	 * Faz o DE/PARA para a mensagem do barramento no form "IA:MCDONALDS:V01:MSG_BARRAMENTO"
	 * @param p_msg_cod CÃ³digo da mensagem 
	 * @return CÃ³digo e descriÃ§Ã£o do erro
	 */
	private String[] getMsgBarramento(String p_msg_cod) {
		String[] ret = new String[2];
		List<HashMap<String, String>> recs = null;
		
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(8, "COD_ERRO");
		fields.put(536870916, "DS_ERRO");
				
		recs = remedyBO.frmQuery("IA:MCDONALDS:V01:MSG_BARRAMENTO", "'7' = 0 AND '8' = \"" + p_msg_cod + "\"", fields);

		if (recs != null && recs.size() > 0) {
			ret[0] = recs.get(0).get("COD_ERRO");
			ret[1] = recs.get(0).get("DS_ERRO");
			
			LOG.info("[getMsgBarramento] COD_ERRO " + ret[0] + " .");
			LOG.info("[getMsgBarramento] DS_ERRO " + ret[1] + " .");
		} else {
			ret[0] = "COD_NAO_ENC";
			ret[1] = "TXT_NAO_ENCONTRADO";
		}
		return ret;
	}
	
	/**
	 * Busca todos os incidentes no Remedy com o Status igual a "McDonalds - Atualizar"
	 * @return Uma lista de incidentes
	 */
	private List<HashMap<String, String>> getIncidentsForReturn() {
		
		LOG.info("[RemedytoMcD] getIncidentsForReturn - Inicio da busca no Remedy");
		List<HashMap<String, String>> ret = null;
		
		HashMap<Integer, String> fields = new HashMap<Integer, String>();
		fields.put(536870925, "VCI_INC_USRSRVREST_INC_ID");
		fields.put(536870930, "VCI_INC_USRSRVREST_INC_REMEDY");
		fields.put(536870948, "VCI_INC_USRSRVREST_INCORIG_ID");
		fields.put(1, "Request ID");
		fields.put(536870945, "VCI_INC_USRSRVREST_NOTIF_STATUS1");
		fields.put(536870933, "VCI_INC_USRSRVREST_HASH");
		fields.put(536870937, "INC-TFL_Descricao_Resolucao");
		fields.put(536870943, "VCI_INC_USRSRVREST_NOTIF_LIST");
		fields.put(536870963, "VCI_INC_USRSRVREST_STATUS_REASON");
		fields.put(7, "Status");
		fields.put(536870958, "NUM_REQUESTS");
					
		ret = remedyBO.frmQuery("IA:MCDONALDS:V01:INCIDENTES_INTEGRADOS", "'7' = \"McDonalds - Atualizar\"", fields); 
		LOG.info("[RemedytoMcD] getIncidentsForReturn - Retornou: "+ret.size()+" Incidentes");
	
		return ret;
	}
	
	/**
	 * Cria um WorkLog na Fujitsu e atualiza o form de integraÃ§Ã£o com o WorkLogID 
	 * @param reqnew ParÃ¢metros do Incidente
	 */
	private boolean createWorklog(HashMap<String, String> reqnew) {
		
		Worklog worklog = new Worklog();
		worklog.setWork_description(reqnew.get("VCI_INC_USRSRVREST_NOTIF_LIST").replace(" ", "_"));
		worklog.setTicket_identifier(reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID"));
		ITMassAPI itMassAPI = new ITMassAPI();
		itMassAPI.init();
		HashMap<String, Object> attrs = null;
		Boolean ret = false;
		
		try {
		
		int worklogID = itMassAPI.addWorklog(worklog);
		
		if(worklogID != 0){
			LOG.info("[createWorklog] - Worklog criado com sucesso id: " +worklogID);	
			attrs = new HashMap<String, Object>();
			attrs.put("VCI_INC_USRSRVREST_NOTIFICACAO", worklogID);
			attrs.put("Status", 4);//Mcdonalds atualizado OK
			remedyBO.updateEntry(Configuration.getConfig("mcd.100.2.1.formIncidentesIntegracao"), reqnew.get("Request ID"), attrs);
			LOG.info("[createWorklog] - Form intermediario atualizado");
			ret = true;
		}
		
		} catch (Exception e){
			
			LOG.error("[createWorklog] - Falha ao criar Worklog no MCD#: " + reqnew.get("VCI_INC_USRSRVREST_INCORIG_ID"));
			ret = false;
			
		}
		return ret;
			
		}
	

}	
