package br.com.ia.oct.mcd.remedy;

public interface RemedyService {
	public void start () throws Exception;
	public void destroy () throws Exception;
	public String getAllMcDonaldsSites(String p_cpyname) throws Exception;
	public String getAllMcDonaldsCMDB(String p_cpyname) throws Exception;
	// public String displayFieldList(String formName) throws Exception;
	// public String formList() throws Exception;
}
